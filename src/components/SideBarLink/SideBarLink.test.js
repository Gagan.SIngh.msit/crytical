import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import SideBarLink from './SideBarLink';

describe('<SideBarLink />', () => {
  test('it should mount', () => {
    render(<SideBarLink />);
    
    const sideBarLink = screen.getByTestId('SideBarLink');

    expect(sideBarLink).toBeInTheDocument();
  });
});