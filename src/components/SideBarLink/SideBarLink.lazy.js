import React, { lazy, Suspense } from 'react';

const LazySideBarLink = lazy(() => import('./SideBarLink'));

const SideBarLink = props => (
  <Suspense fallback={null}>
    <LazySideBarLink {...props} />
  </Suspense>
);

export default SideBarLink;
