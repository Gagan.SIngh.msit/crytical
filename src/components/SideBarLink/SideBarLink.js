import React from 'react';
import PropTypes from 'prop-types';
import styles from './SideBarLink.module.scss';
import { Link as RouterLink } from 'react-router-dom';
import { Text, Link, Flex } from '@chakra-ui/react';
import { act } from '@testing-library/react';
const SideBarLink = ({ text, active, href, isdisabled, activeSec }) => {
  if (isdisabled === true) {
    return (
      <Flex
        direction="column"
        backdropFilter="blur(0px)"
        width="100%"
        height="59px"
        paddingLeft="18px"
        className='disabled-text'
      >
        <Text >{text}</Text>
        <Text
          fontSize="10px"
          fontWeight="600">COMING SOON</Text>
      </Flex>
    );
  } else {
    return (

      <Link
        as={RouterLink}

        backdropFilter={active === true ? "none" : "blur(0px)"}
        width="100%"
        height="59px"
        paddingLeft="18px"
        to={href}
      >
        <Flex alignItems="center" height="100%">
          <Text className={active === true ? "active-text" : "normal-text"}>
            {text}
          </Text>
        </Flex>

      </Link>
    );
  }

}
SideBarLink.propTypes = {};

SideBarLink.defaultProps = {};

export default SideBarLink;
