import React, { lazy, Suspense } from 'react';

const LazyCryticalProTable = lazy(() => import('./CryticalProTable'));

const CryticalProTable = props => (
  <Suspense fallback={null}>
    <LazyCryticalProTable {...props} />
  </Suspense>
);

export default CryticalProTable;
