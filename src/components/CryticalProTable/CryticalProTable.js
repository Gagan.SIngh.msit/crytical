import React from 'react';
import PropTypes from 'prop-types';
import styles from './CryticalProTable.module.scss';
import { TableContainer, Thead, Tr, Th, Tbody, Table, Td, Flex } from '@chakra-ui/react';
const CryticalProTable = ({ data, labels }) => (
  <Flex flex="1" className={styles.CryticalProTable} data-testid="CryticalProTable">
    <TableContainer width="100%">
      <Table variant='unstyled'>
        <Thead className={styles.CryticalProTable__TableHeader}>
          <Tr>
            {labels.map((tx) => <Th padding={{ base: "5px 5px", md: "var(--chakra-space-3) var(--chakra-space-6)" }}>{tx}</Th>)}
          </Tr>
        </Thead>
        <Tbody>
          {data.map((tx) =>
            <Tr className={styles.CryticalProTable__TableRow}>
              {tx.map((r) => <Td fontSize={{ base: "7px", md: "14px" }}>{r}</Td>)}
            </Tr>)}
        </Tbody>
      </Table>
    </TableContainer>
  </Flex >
);

CryticalProTable.propTypes = {};

CryticalProTable.defaultProps = {};

export default CryticalProTable;
