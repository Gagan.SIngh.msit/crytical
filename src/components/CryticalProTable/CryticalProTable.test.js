import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import CryticalProTable from './CryticalProTable';

describe('<CryticalProTable />', () => {
  test('it should mount', () => {
    render(<CryticalProTable />);
    
    const cryticalProTable = screen.getByTestId('CryticalProTable');

    expect(cryticalProTable).toBeInTheDocument();
  });
});