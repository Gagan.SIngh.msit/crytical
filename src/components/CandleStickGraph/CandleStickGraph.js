import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import styles from './CandleStickGraph.module.scss';
import Chart from "react-apexcharts";
import { Box, Flex, Text } from '@chakra-ui/react';

const CandleStickGraph = ({ title, labels, values, values2 }) => {
  let [state, setState] = useState({
    options: {
      chart: {
        id: "candlestick",
        toolbar: {
          show: false
        }
      },

    },
    series: [
      {
        name: "series-1",
        data: []
      }
    ]
  });
  const updateGraph = () => {
    let data = [];
    for (let i = 0; i < labels.length; ++i) {
      data.push({
        x: labels[i],
        y: values[i]
      });
    }
    setState((prevState) => ({
      ...prevState,
      series: [
        {
          name: "series-1",
          data: data
        }
      ]
    }));
  }
  useEffect(updateGraph, [labels, values]);
  return (
    <Flex direction="column" width="100%" height="100%">
      <Flex justifyContent="space-between" alignItems="center">
        <Text
          fontStyle="var(--unnamed-font-style-normal) normal var(--unnamed-font-weight-medium) var(--unnamed-font-size-30)/var(--unnamed-line-spacing-46) var(--unnamed-font-family-poppins)"
          fontSize={{ base: "10px", md: "22px" }}
          color="var(--unnamed-color-565555)"
          fontWeight="bold">{title}</Text>
        <Box>
          <button
            className={styles.CandleStickGraph__ButtonStyle}
            styles={{ boxShadow: "inset -2px -2px 3px #FFFFFF6A" }}>7D</button>
          <button
            className={styles.CandleStickGraph__ButtonStyle}
            styles={{ boxShadow: "inset -2px -2px 3px #FFFFFF6A" }}
          >30D</button>
          <button
            className={styles.CandleStickGraph__ButtonStyle}
            styles={{ boxShadow: "inset -2px -2px 3px #FFFFFF6A" }}>All</button>
        </Box>
      </Flex>
      <Chart
        options={state.options}
        series={state.series}
        type="candlestick"
        width="100%"
      />
    </Flex>
  );
}

CandleStickGraph.propTypes = {};

CandleStickGraph.defaultProps = {};

export default CandleStickGraph;
