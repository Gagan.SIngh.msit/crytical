import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import CandleStickGraph from './CandleStickGraph';

describe('<CandleStickGraph />', () => {
  test('it should mount', () => {
    render(<CandleStickGraph />);
    
    const candleStickGraph = screen.getByTestId('CandleStickGraph');

    expect(candleStickGraph).toBeInTheDocument();
  });
});