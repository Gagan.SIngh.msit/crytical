import React, { lazy, Suspense } from 'react';

const LazyCandleStickGraph = lazy(() => import('./CandleStickGraph'));

const CandleStickGraph = props => (
  <Suspense fallback={null}>
    <LazyCandleStickGraph {...props} />
  </Suspense>
);

export default CandleStickGraph;
