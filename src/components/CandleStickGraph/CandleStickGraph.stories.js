/* eslint-disable */
import CandleStickGraph from './CandleStickGraph';

export default {
  title: "CandleStickGraph",
};

export const Default = () => <CandleStickGraph />;

Default.story = {
  name: 'default',
};
