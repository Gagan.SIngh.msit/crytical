import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import BondsInfoBox from './BondsInfoBox';

describe('<BondsInfoBox />', () => {
  test('it should mount', () => {
    render(<BondsInfoBox />);
    
    const bondsInfoBox = screen.getByTestId('BondsInfoBox');

    expect(bondsInfoBox).toBeInTheDocument();
  });
});