import React from 'react';
import { Flex, Text } from '@chakra-ui/react';

const BondsInfoBox = ({ title, subtitle }) => (
  <Flex
    direction="column"
    justifyContent="space-between"
    padding={{ base: "35px 18px 35px 18px", md: "35px" }}
    color="#565555">
    <Text
      fontSize={{ base: "16px", md: "18px" }}>{title}</Text>
    <Text
      paddingTop="22px"
      fontSize={{ base: "20px", md: "22px" }}>{subtitle}</Text>
  </Flex>

);

BondsInfoBox.propTypes = {};

BondsInfoBox.defaultProps = {};

export default BondsInfoBox;
