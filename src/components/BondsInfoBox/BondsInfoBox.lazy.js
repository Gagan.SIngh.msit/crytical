import React, { lazy, Suspense } from 'react';

const LazyBondsInfoBox = lazy(() => import('./BondsInfoBox'));

const BondsInfoBox = props => (
  <Suspense fallback={null}>
    <LazyBondsInfoBox {...props} />
  </Suspense>
);

export default BondsInfoBox;
