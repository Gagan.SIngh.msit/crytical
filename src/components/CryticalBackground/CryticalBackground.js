import React from 'react';
import PropTypes from 'prop-types';
import styles from './CryticalBackground.module.scss';
import { Box, Image } from '@chakra-ui/react';
import FirstImage from "../../assets/images/onePager_Buble_1.png";
import ThirdImage from "../../assets/images/onePager_Buble_3.png";
const CryticalBackground = () => (
  <Box
    className={styles.CryticalBackground}
    data-testid="CryticalBackground"
    background="transparent url('/img/background.png') 0% 0% no-repeat padding-box"
    backgroundSize="cover"
    width="100%"
    height="100vh"
    pos="fixed"
    top="0px"
    left="0px"
    zIndex="-1"
  >
    <Image
      position="absolute"
      display={{ base: "none", md: "block" }}
      right="0px"
      top="100px"
      height="200px"
      src={FirstImage} />
    <Image
      position="absolute"
      display={{ base: "none", md: "block" }}
      top="150px"
      left="173px"
      width="333px"
      src={ThirdImage}
    />
  </Box>
);

CryticalBackground.propTypes = {};

CryticalBackground.defaultProps = {};

export default CryticalBackground;
