import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import CryticalBackground from './CryticalBackground';

describe('<CryticalBackground />', () => {
  test('it should mount', () => {
    render(<CryticalBackground />);
    
    const cryticalBackground = screen.getByTestId('CryticalBackground');

    expect(cryticalBackground).toBeInTheDocument();
  });
});