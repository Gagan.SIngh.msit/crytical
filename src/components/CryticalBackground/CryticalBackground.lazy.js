import React, { lazy, Suspense } from 'react';

const LazyCryticalBackground = lazy(() => import('./CryticalBackground'));

const CryticalBackground = props => (
  <Suspense fallback={null}>
    <LazyCryticalBackground {...props} />
  </Suspense>
);

export default CryticalBackground;
