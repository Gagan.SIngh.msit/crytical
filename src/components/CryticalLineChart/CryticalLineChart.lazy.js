import React, { lazy, Suspense } from 'react';

const LazyCryticalLineChart = lazy(() => import('./CryticalLineChart'));

const CryticalLineChart = props => (
  <Suspense fallback={null}>
    <LazyCryticalLineChart {...props} />
  </Suspense>
);

export default CryticalLineChart;
