import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import CryticalLineChart from './CryticalLineChart';

describe('<CryticalLineChart />', () => {
  test('it should mount', () => {
    render(<CryticalLineChart />);
    
    const cryticalLineChart = screen.getByTestId('CryticalLineChart');

    expect(cryticalLineChart).toBeInTheDocument();
  });
});