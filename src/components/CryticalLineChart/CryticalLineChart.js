import React from 'react';
import PropTypes from 'prop-types';
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend,
} from 'chart.js';
import { Line } from 'react-chartjs-2';

import styles from './CryticalLineChart.module.scss';
import { Box, Button, Flex, Text } from '@chakra-ui/react';

ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend
);
const CryticalLineChart = ({ title, labels, values, values2 }) => {

  const buttonStyling = { base: "20px", md: "30px" };
  const fontStyle = { base: "8px", md: "var(--chakra-fontSizes-md)" };

  const options = {
    responsive: true,
    plugins: {
      legend: {
        position: 'top',
      },
      title: {
        display: true,
        text: '',
      },
    },
  };

  const data = {
    labels,
    datasets: [
      {
        label: 'Dataset 1',
        data: values,
        borderColor: 'rgb(255, 99, 132)',
        backgroundColor: 'rgba(255, 99, 132, 0.5)',
      },
      {
        label: 'Dataset 2',
        data: values2,
        borderColor: 'rgb(53, 162, 235)',
        backgroundColor: 'rgba(53, 162, 235, 0.5)',
      },
    ],
  };
  return (
    <Flex direction="column" width="100%" height="100%">
      <Flex justifyContent="space-between" alignItems="center">
        <Text
          fontStyle="var(--unnamed-font-style-normal) normal var(--unnamed-font-weight-medium) var(--unnamed-font-size-30)/var(--unnamed-line-spacing-46) var(--unnamed-font-family-poppins)"
          fontSize={{ base: "10px", md: "22px" }}
          color="var(--unnamed-color-565555)"
          fontWeight="bold">{title}</Text>
        <Box>
          <button
            className={styles.CryticalLineChart__ButtonStyle}
            styles={{ boxShadow: "inset -2px -2px 3px #FFFFFF6A" }}>7D</button>
          <button
            className={styles.CryticalLineChart__ButtonStyle}
            styles={{ boxShadow: "inset -2px -2px 3px #FFFFFF6A" }}
          >30D</button>
          <button
            className={styles.CryticalLineChart__ButtonStyle}
            styles={{ boxShadow: "inset -2px -2px 3px #FFFFFF6A" }}>All</button>
        </Box>
      </Flex>
      <Line maxWidth="300%" options={options} data={data} />
    </Flex>
  );
}

CryticalLineChart.propTypes = {};

CryticalLineChart.defaultProps = {};

export default CryticalLineChart;
