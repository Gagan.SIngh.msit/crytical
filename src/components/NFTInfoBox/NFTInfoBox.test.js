import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import NftInfoBox from './NftInfoBox';

describe('<NftInfoBox />', () => {
  test('it should mount', () => {
    render(<NftInfoBox />);
    
    const nftInfoBox = screen.getByTestId('NftInfoBox');

    expect(nftInfoBox).toBeInTheDocument();
  });
});