import React, { lazy, Suspense } from 'react';

const LazyNftInfoBox = lazy(() => import('./NftInfoBox'));

const NftInfoBox = props => (
  <Suspense fallback={null}>
    <LazyNftInfoBox {...props} />
  </Suspense>
);

export default NftInfoBox;
