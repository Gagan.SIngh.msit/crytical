import React from 'react';
import { Flex, Text, Box } from '@chakra-ui/react';
import { CopyIcon } from '@chakra-ui/icons';



const NftInfoBox = ({ title, subtitle }) => (
  <Flex
    background="transparent 0% 0% no-repeat padding-box"
    direction="column"
    width={{ base: "164px", md: "220px", lg: "275px" }}
    height={{ base: "auto", md: "auto", lg: "182px" }}
    border="1px solid #FFFFFF26"
    borderRadius="20px"
    backdropFilter="brightness(.96)"
    boxShadow="inset 0px 3px 6px #00000029"
    padding={{ base: "27px 17px", md: "36.55px 54.54px" }}
  >
    <Text
      textAlign="center"
      color="#565555"
      fontFamily="'Poppins', sans-serif"
      fontWeight="500"
      fontSize={{ base: "12px", md: "18" }}
    >{title}</Text>
    <Box
      height={{ base: "4px", md: "6px", lg: "6px" }}
      width={{ base: "113px", md: "133px", lg: "189px" }}
      background="transparent linear-gradient(274deg, #FF9AE8 0%, #BDFD9A 100%) 0% 0% no-repeat padding-box"
    ></Box>
    <Flex
      direction="row"
      paddingTop="17px"
      position="relative"
      left="6px"
    >
      <Text
        textAlign="center"
        color="#565555"
        fontFamily="'Poppins', sans-serif"
        fontSize={{ base: "10px", md: "13px", lg: "16px" }}
      >
        {subtitle}
      </Text>
      <CopyIcon
        color="#565555"
        fontFamily="'Poppins', sans-serif"
        height="18px"
        marginLeft={{ base: "0px", md: "22px" }}
      />
    </Flex>

  </Flex>
);

NftInfoBox.propTypes = {};

NftInfoBox.defaultProps = {};

export default NftInfoBox;
