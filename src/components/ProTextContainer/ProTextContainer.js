import React from 'react';
import PropTypes from 'prop-types';
import styles from './ProTextContainer.module.scss';
import { Flex, Text } from '@chakra-ui/react';
const ProTextContainer = ({ title, subtitle, width }) => (
  <Flex
    background="transparent 0% 0% no-repeat padding-box" direction="column"
    boxShadow="inset 0px 3px 6px #00000029"
    border="1px solid #FFFFFF26"
    borderRadius="20px"
    opacity="1"
    backdropFilter="brightness(.96)"
    padding={{ base: "25px", md: "27px 36px" }}
    height={{ base: "100px", md: "127px", xl: "160px" }}
    width={width}
    overflow="hidden">
    <Text className={styles.ProTextContainer__title} fontSize={
      width === "100%" ?
        { base: "16px", md: "20px" } : { base: "10px", md: "20px" }
    }>{title}</Text>
    <Text className={styles.ProTextContainer__subtitle} fontSize={
      width === "100%" ?
        { base: "15px", md: "16px" }
        : { base: "10px", md: "16px" }}>{subtitle}</Text>
  </Flex>
);

ProTextContainer.propTypes = {};

ProTextContainer.defaultProps = {};

export default ProTextContainer;
