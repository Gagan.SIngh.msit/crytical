import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import ProTextContainer from './ProTextContainer';

describe('<ProTextContainer />', () => {
  test('it should mount', () => {
    render(<ProTextContainer />);
    
    const proTextContainer = screen.getByTestId('ProTextContainer');

    expect(proTextContainer).toBeInTheDocument();
  });
});