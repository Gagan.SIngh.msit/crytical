import React, { lazy, Suspense } from 'react';

const LazyProTextContainer = lazy(() => import('./ProTextContainer'));

const ProTextContainer = props => (
  <Suspense fallback={null}>
    <LazyProTextContainer {...props} />
  </Suspense>
);

export default ProTextContainer;
