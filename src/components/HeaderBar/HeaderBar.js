import React from 'react';
import PropTypes from 'prop-types';
import styles from './HeaderBar.module.scss';

const HeaderBar = () => (
  <div className={styles.HeaderBar} data-testid="HeaderBar">
    HeaderBar Component
  </div>
);

HeaderBar.propTypes = {};

HeaderBar.defaultProps = {};

export default HeaderBar;
