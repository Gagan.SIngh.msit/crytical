import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import HomeLabel from './HomeLabel';

describe('<HomeLabel />', () => {
  test('it should mount', () => {
    render(<HomeLabel />);
    
    const homeLabel = screen.getByTestId('HomeLabel');

    expect(homeLabel).toBeInTheDocument();
  });
});