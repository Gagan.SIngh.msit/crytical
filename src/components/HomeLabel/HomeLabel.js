import React from 'react';
import PropTypes from 'prop-types';
import styles from './HomeLabel.module.scss';
import { Flex, Text } from '@chakra-ui/react';

const HomeLabel = ({ title, left_title, right_title }) => (
  <Flex width={{ base: "100%", lg: "33%" }} direction="column">
    <Text
      fontStyle="normal normal 600 12px/40px Poppins"
      letterSpacing="0.24px"
      color="#565555"
      position="relative"
      top={{ base: "30px", md: 0 }}
      paddingLeft={{ base: "20px", md: "0" }}
      zIndex="1"
      fontWeight={{ base: "500", md: "bold" }}
      overflow="auto"
      whiteSpace="nowrap">{title}</Text>
    <Flex
      flexDirection="row"
      alignItems="center"
      background="transparent 0% 0% no-repeat padding-box"
      boxShadow="inset 0px 3px 6px #00000029"
      border="1px solid #FFFFFF26"
      borderRadius="20px"
      justifyContent="space-between"
      backdropFilter="brightness(17px)"
      width="100%"
      height="69px"
      padding="0px 20px 0px 20px"
      overflow="auto">
      <Text
        fontStyle="var(--unnamed-font-style-normal) normal var(--unnamed-font-weight-bold) 17px/26px var(--unnamed-font-family-poppins)"
        letterSpacing="var(--unnamed-character-spacing-0)"
        color="#FF7495"
        fontWeight="bold"
        vertical-align="middle"
        position="relative"
        top={{ base: "10px", md: "0px" }}>{left_title}</Text>
      <Text
        fontStyle="var(--unnamed-font-style-normal) normal var(--unnamed-font-weight-medium) var(--unnamed-font-size-15)/23px var(--unnamed-font-family-poppins)"
        letterSpacing="var(--unnamed-character-spacing-0)"
        textAlign="left"
        fontWeight="bold"
        color="#BABABA">{right_title}</Text>
    </Flex>
  </Flex>
);

HomeLabel.propTypes = {};

HomeLabel.defaultProps = {};

export default HomeLabel;
