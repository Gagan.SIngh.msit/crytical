import React, { lazy, Suspense } from 'react';

const LazyHomeLabel = lazy(() => import('./HomeLabel'));

const HomeLabel = props => (
  <Suspense fallback={null}>
    <LazyHomeLabel {...props} />
  </Suspense>
);

export default HomeLabel;
