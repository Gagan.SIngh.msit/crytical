import React, { lazy, Suspense } from 'react';

const LazyLaunchPadCard = lazy(() => import('./LaunchPadCard'));

const LaunchPadCard = props => (
  <Suspense fallback={null}>
    <LazyLaunchPadCard {...props} />
  </Suspense>
);

export default LaunchPadCard;
