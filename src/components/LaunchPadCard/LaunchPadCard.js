import React from 'react';
import PropTypes from 'prop-types';
import styles from './LaunchPadCard.module.scss';
import { Flex, Text, Box, Progress } from '@chakra-ui/react';
const LaunchPadCard = ({ type }) => (
  <Flex direction="column"
    className={styles.LaunchPadCard}
    width={{ base: "100%", md: "350px", xl: "335px" }}>
    <Flex direction="row">
      <Text className={styles.LaunchPadCard__Title}>
        Lorem ipsum dolor sit amet title title …tile
      </Text>
      <Flex direction="column" paddingLeft="5px">
        <Text className={styles.LaunchPadCard__Status}
          fontSize={{ base: "13px", md: "15px" }}
          color={type === 'live' ? "#A0DF86" : type === "upcoming" ? "#155AC1" : "#FF7495"}
        >{type}</Text>
        <Text className={styles.LaunchPadCard__Index}>#16</Text>
      </Flex>
    </Flex>
    <Flex alignItems="center">
      <Box className={styles.LaunchPadCard__Circle}></Box>
      <Text className={styles.LaunchPadCard__MVC}>MVC</Text>
    </Flex>
    <Flex width="100%" alignItems="center">
      {type === 'live' ?
        <Progress colorScheme="progressGreen" flex="1" value={50} height="5px" background="rgba(160, 223, 134, 0.3)" />
        :
        type === "upcoming" ?
          <Progress colorScheme="progressBlue" flex="1" value={50} height="5px" background="rgba(21, 90, 193, 0.3)" />
          :
          <Progress colorScheme="progressPink" flex="1" value={50} height="5px" background="rgba(255, 116, 149, 0.3)" />}

      <Text className={styles.LaunchPadCard__ProgressText}
        fontSize={{ base: "11px", md: "16px" }}>STATUS</Text>
    </Flex>
    <Flex>
      <Text color="#4B434B" letterSpacing="0" fontSize="12px">04.14, 11 AM</Text>
    </Flex>
    <Flex flex="1" justifyContent="space-between" color="#4B434B" fontSize={{ base: "13px", md: "14px" }}>
      <Flex direction="column" justifyContent="space-evenly">
        <Text
          fontWeight="600"
          fontSize={{ base: "15px", md: "18px" }}
          letterSpacing="0px"
        >TOTAL RAISED</Text>
        <Text fontSize="14px">Auction Currency</Text>
        <Text>Price per unit, $</Text>
        <Text>Listing time</Text>
      </Flex>
      <Flex direction="column" justifyContent="space-evenly">
        <Text fontSize={{ base: "15px", md: "18px" }}>1000 ROSE</Text>
        <Text>ROSE</Text>
        <Text>0.01</Text>
        <Text>TBA</Text>
      </Flex>
    </Flex>
  </Flex>
);

LaunchPadCard.propTypes = {};

LaunchPadCard.defaultProps = {};

export default LaunchPadCard;
