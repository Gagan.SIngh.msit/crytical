import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import LaunchPadCard from './LaunchPadCard';

describe('<LaunchPadCard />', () => {
  test('it should mount', () => {
    render(<LaunchPadCard />);
    
    const launchPadCard = screen.getByTestId('LaunchPadCard');

    expect(launchPadCard).toBeInTheDocument();
  });
});