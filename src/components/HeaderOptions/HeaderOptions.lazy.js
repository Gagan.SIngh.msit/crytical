import React, { lazy, Suspense } from 'react';

const LazyHeaderOptions = lazy(() => import('./HeaderOptions'));

const HeaderOptions = props => (
  <Suspense fallback={null}>
    <LazyHeaderOptions {...props} />
  </Suspense>
);

export default HeaderOptions;
