import React, { useState } from 'react';
import PropTypes from 'prop-types';
import styles from './HeaderOptions.module.scss';
import { Flex, Button, Text, Select, useDisclosure, Image, } from '@chakra-ui/react';
import { Icon } from '@iconify/react';
import { DownOutlined, WalletOutlined } from '@ant-design/icons';
import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  Box
} from '@chakra-ui/react';
import WalletRows from '../WalletRows/WalletRows';
import { Dropdown, Menu, Space } from 'antd';
import EarthIcon from "../../assets/icons/global.svg";

import WalletConnect from "../../assets/images/walletconnect.png";
import MetaMask from "../../assets/images/metamask.png";
import TrustWallet from "../../assets/images/trust-wallet.png";
import Coinbase from "../../assets/images/coinbase.png";


const HeaderOptions = () => {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [num, setNum] = useState(null);
  const [item, setItem] = useState(null);
  const [currentLanguage, setCurrentLanguage] = useState('ENG');
  const [languageMenu, setLanguageMenu] = useState(
    [
      {
        key: '1',
        label: "한국어",
      }
    ]);
  const handleClick = ({ key }) => {
    if (key === '1') {
      setLanguageMenu([
        {
          key: '2',
          label: "ENG",
        }
      ]);
      setCurrentLanguage("한국어");
    } else {
      setLanguageMenu([
        {
          key: '1',
          label: "한국어",
        }
      ]);
      setCurrentLanguage("ENG");
    }
    //you can perform setState here
  }
  const registerWallet = () => {
    if (num === null) return;
    if (item === null) {
      setItem(num);
    } else {
      setNum(null);
      setItem(null);
    }
  }

  return (
    <>
      <Flex
        display={{ base: "none", md: "flex" }}
        width="100%"
        height="95px"
        alignItems="center"
        paddingRight="58px"
        flexDirection="row"
      >
        <Button
          marginLeft="auto"
          width="199px"
          height="45px"
          background="#E9E6E7 0% 0% no-repeat padding-box"
          boxShadow="-2px -2px 3px #FFFFFF65"
          borderRadius="4px"
          opacity="1"
          alignItems="center"
          onClick={onOpen}
        >
          <Flex>
            <WalletOutlined
              style={{ color: '#231F20' }} />
            <Text
              paddingLeft="10px"
              fontStyle="var(--unnamed-font-style-normal) normal var(--unnamed-font-weight-normal) 16px/25px var(--unnamed-font-family-poppins)"
              color="#231F20"
              fontFamily="'Poppins', sans-serif"
            >Connect Wallet
            </Text>
          </Flex>
        </Button>

        <Dropdown overlay={
          <Menu
            onClick={handleClick}
            items={languageMenu}
            style={{
              backgroundColor: "transparent",
              textAlign: "right"
            }}
          />
        }>
          <Flex
            marginLeft="15px"
            alignItems="center">
            <Image paddingRight="7px" width="25px" height="25px" src={EarthIcon} />
            <Text
              fontSize="18px"
              color="#231F20"
              fontWeight="500">{currentLanguage}</Text>
            <DownOutlined
              style={{
                paddingLeft: "5px",
                color: "#231F20",
                fontSize: "15px",
                fontWeight: "500"
              }} />
          </Flex>
        </Dropdown>

      </Flex>
      <Modal isOpen={isOpen} onClose={onClose}>
        <ModalOverlay />
        <ModalContent
          background="transparent linear-gradient(221deg, #FF7495 0%, #A0DF86 100%) 0% 0% no-repeat padding-box"
          maxWidth="645px"
          borderRadius="20px"
          boxShadow="3px 3px 6px #00000029"
          overflow="none"
          padding="2px"
        >
          <Flex
            borderRadius="20px"
            direction="column"
            background="transparent linear-gradient(221deg, #EBE5E5 0%, #E3DDE1 86%, #EAE4E4 100%) 0% 0% no-repeat padding-box"
          >

            <ModalHeader
              paddingLeft="42px"
              paddingRight="36px"
              paddingTop="49px" fontFamily="'Poppins', sans-serif" textDecoration="underline">Connect to a Wallet</ModalHeader>

            <ModalCloseButton />
            <ModalBody
              paddingLeft="42px"
              paddingRight="36px"
              width="645px">

              <Flex
                direction="column" width="100%"
                gap="38px">
                <WalletRows ukey="1" item={item} activeKey={num} setActive={setNum} image={WalletConnect} title="WalletConnect" />
                <WalletRows ukey="2" item={item} activeKey={num} setActive={setNum} image={MetaMask} title="Metamask" />
                <WalletRows ukey="3" item={item} activeKey={num} setActive={setNum} image={TrustWallet} title="TrustWallet" />
                <WalletRows ukey="4" item={item} activeKey={num} setActive={setNum} image={Coinbase} title="Coinbase" />
              </Flex>
            </ModalBody>
            <ModalFooter
              paddingBottom="56px">
              <Flex
                width="100%"
                justifyContent="center">

                <Button
                  width="175px"
                  height="45px"
                  background={
                    item !== null ?
                      "#A0DF86 0% 0% no-repeat padding-box" :
                      num === null ? "#D6D6D6 0% 0% no-repeat padding-box" : "#FF7495 0% 0% no-repeat padding-box"}
                  color="#FFFFFF"
                  fontSize="14px"
                  onClick={registerWallet}>
                  {item !== null ? "Disconnect" : "Connect"}

                </Button>
              </Flex>
            </ModalFooter>
          </Flex>
        </ModalContent>
      </Modal>

    </>
  );
}

HeaderOptions.propTypes = {};

HeaderOptions.defaultProps = {};

export default HeaderOptions;
