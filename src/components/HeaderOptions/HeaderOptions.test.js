import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import HeaderOptions from './HeaderOptions';

describe('<HeaderOptions />', () => {
  test('it should mount', () => {
    render(<HeaderOptions />);
    
    const headerOptions = screen.getByTestId('HeaderOptions');

    expect(headerOptions).toBeInTheDocument();
  });
});