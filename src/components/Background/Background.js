import React from 'react';
import PropTypes from 'prop-types';
import './Background.scss';

const Background = () => (
  <div className="Background" data-testid="Background">
    Background Component
  </div>
);

Background.propTypes = {};

Background.defaultProps = {};

export default Background;
