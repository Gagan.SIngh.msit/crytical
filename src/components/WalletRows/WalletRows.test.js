import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import WalletRows from './WalletRows';

describe('<WalletRows />', () => {
  test('it should mount', () => {
    render(<WalletRows />);
    
    const walletRows = screen.getByTestId('WalletRows');

    expect(walletRows).toBeInTheDocument();
  });
});