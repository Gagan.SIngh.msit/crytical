import React, { lazy, Suspense } from 'react';

const LazyWalletRows = lazy(() => import('./WalletRows'));

const WalletRows = props => (
  <Suspense fallback={null}>
    <LazyWalletRows {...props} />
  </Suspense>
);

export default WalletRows;
