import React from 'react';
import PropTypes from 'prop-types';
import styles from './WalletRows.module.scss';
import { Flex, Box, Text, Image } from "@chakra-ui/react";

const WalletRows = ({ ukey, item, activeKey, setActive, title, image }) => {
  console.log(activeKey, ukey);
  return (
    <Flex
      onClick={() => { setActive(ukey); }}
      alignItems="center"
      padding="0px 17px"
      width="567px"
      height="50px"
      borderRadius="10px"
      background={ukey === item ? "transparent linear-gradient(98deg, #FF7495 0%, #A0DF86 100%) 0% 0% no-repeat padding-box" : ukey === activeKey ? "#FFF" :
        "transparent linear-gradient(95deg, #23009639 0%, #5D53534D 100%) 0% 0% no-repeat padding-box"
      }>
      <Box
        width="32px"
        height="32px"
        borderRadius="50%"
      >
        <Image
          width="32px"
          height="32px"
          src={image}
        />
      </Box>
      <Text
        fontSize="16px"
        color="#4B434B"
        paddingLeft="26px">
        {title}
      </Text>
    </Flex>
  );
}

WalletRows.propTypes = {};

WalletRows.defaultProps = {};

export default WalletRows;
