import React, { lazy, Suspense } from 'react';

const LazySideBarDetail = lazy(() => import('./SideBarDetail'));

const SideBarDetail = props => (
  <Suspense fallback={null}>
    <LazySideBarDetail {...props} />
  </Suspense>
);

export default SideBarDetail;
