import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import SideBarDetail from './SideBarDetail';

describe('<SideBarDetail />', () => {
  test('it should mount', () => {
    render(<SideBarDetail />);
    
    const sideBarDetail = screen.getByTestId('SideBarDetail');

    expect(sideBarDetail).toBeInTheDocument();
  });
});