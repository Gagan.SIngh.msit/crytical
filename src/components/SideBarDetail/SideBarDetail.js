import React from 'react';
import PropTypes from 'prop-types';
import styles from './SideBarDetail.module.scss';
import { Container, Flex, Text } from '@chakra-ui/react';
import { TriangleUpIcon } from '@chakra-ui/icons';

const SideBarDetail = ({ title, left_title, right_title }) => (
  <Container height="100px">
    <Text
      className="disabled-text"
      font="normal normal medium 14px/21px Poppins"
    >{title}</Text>
    <Flex width="100%" direction="row" justifyContent="space-between">
      <Text
        fontWeight="bold"
        letterSpacing="var(--unnamed-character-spacing-0)"
        color="#231F20"
        fontStyle="var(--unnamed-font-style-normal) normal var(--unnamed-font-weight-bold) var(--unnamed-font-size-12)/18px var(--unnamed-font-family-poppins)"
        textAlign="left">{left_title}</Text>
      {right_title != null && right_title != undefined ?
        <Text
          fontStyle="var(--unnamed-font-style-normal) normal var(--unnamed-font-weight-bold) 10px/16px var(--unnamed-font-family-poppins)"
          textAlign="right"
          color="#3DC405"
          fontWeight="bold"
        ><TriangleUpIcon /> {right_title}</Text> : null}
    </Flex>
    <Container background="transparent linear-gradient(90deg, #FF9AE8 0%, #BDFD9A 100%) 0% 0% no-repeat padding-box"
      height="2px" />
  </Container>
);

SideBarDetail.propTypes = {};

SideBarDetail.defaultProps = {};

export default SideBarDetail;
