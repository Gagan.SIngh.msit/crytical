import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import CryticalTable from './CryticalTable';

describe('<CryticalTable />', () => {
  test('it should mount', () => {
    render(<CryticalTable />);
    
    const cryticalTable = screen.getByTestId('CryticalTable');

    expect(cryticalTable).toBeInTheDocument();
  });
});