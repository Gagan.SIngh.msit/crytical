import React from 'react';
import styles from './CryticalTable.module.scss';
import { Box, Text, Flex, TableContainer, Thead, Table, Tr, Th, Tbody, Td, Image, } from '@chakra-ui/react';

import BlackNFTImage from "../../assets/images/CRYTICAL_NFT_Black.png";
import DiamondNFTImage from "../../assets/images/CRYTICAL_NFT_Diamond.png";
import GoldNFTImage from "../../assets/images/CRYTICAL_NFT_Gold.png";
import SilverNFTImage from "../../assets/images/CRYTICAL_NFT_Silver.png";
import BronzeNFTImage from "../../assets/images/BRONZE_Crop.png";

const CryticalTable = ({ width, data, labels, stretch, type }) => {
  const returnImage = () => {
    if (type === "Diamond") {
      return DiamondNFTImage;
    } else if (type === "Gold") {
      return GoldNFTImage;
    } else if (type === "Silver") {
      return SilverNFTImage;
    } else if (type === "Bronze") {
      return BronzeNFTImage;
    }
    return BlackNFTImage;
  }
  return (
    <Box
      flex="0"
      background="transparent 0% 0% no-repeat padding-box"
      border="1px solid #FFFFFF26"
      borderRadius="20px"
      minWidth="200px"
      boxShadow="inset 0px 3px 6px #00000029"
      width={width !== null && width !== undefined ? width : { base: "100%", xl: "550px", "2xl": "100%" }}
      marginTop="30px"
      borderBottomLeftRadius={{ base: "20px", md: "0px" }}
      borderTopLeftRadius={{ base: "20px", md: "0px" }}
    >
      <Flex direction={{ base: "column", md: "row" }}>
        <Flex
          direction="column"
          position="relative"
          left="-30px"
          display={{ base: "none", md: "flex" }}
        >
          <Image maxH="161px" fit="contain" src={returnImage()} />

          <Text className={
            `${styles.CryticalTable__LogoText} ${type === "Diamond" ? styles.CryticalTable__LogoText__Diamond :
              type === "Gold" ? styles.CryticalTable__LogoText__Gold :
                type === "Silver" ? styles.CryticalTable__LogoText__Silver :
                  type === "Bronze" ? styles.CryticalTable__LogoText__Bronze : ""
            }`}>
            {type !== null && type !== undefined ? type : "black"}

          </Text>
        </Flex>
        <Text
          display={{ base: "inline-block", md: "none" }}
          width="100%"
          paddingLeft="18px"
          paddingTop="18px"
          fontWeight="800"
          fontSize="17px"
          color={type === "Diamond" ? "#A3A7AF" :
            type === "Gold" ? "#D6B545" :
              type === "Silver" ? "#8B8B89" :
                type === "Bronze" ? "#CD5E0B"
                  : "#000000"}
          textTransform="uppercase">{type !== null && type !== undefined ? type : "black"}</Text>
        <TableContainer >
          <Table variant='unstyled'>
            <Thead className={styles.CryticalTable__TableHeader}>
              <Tr>
                {labels.map((tx) =>
                  <Th fontWeight="600" fontSize={{ base: "6px", md: "14px" }}>
                    {tx}
                  </Th>)}
              </Tr>
            </Thead>
            <Tbody>
              {data.map((tx) =>
                <Tr className={styles.CryticalTable__TableRow}>
                  {tx.map((r) => <Td fontSize={{ base: "7px", md: "14px" }}>{r}</Td>)}
                </Tr>)}
            </Tbody>
          </Table>
        </TableContainer >
      </Flex>
    </Box >
  );
}

CryticalTable.propTypes = {};

CryticalTable.defaultProps = {};

export default CryticalTable;
