import React, { lazy, Suspense } from 'react';

const LazyCryticalTable = lazy(() => import('./CryticalTable'));

const CryticalTable = props => (
  <Suspense fallback={null}>
    <LazyCryticalTable {...props} />
  </Suspense>
);

export default CryticalTable;
