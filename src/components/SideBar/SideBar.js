import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import styles from './SideBar.module.scss';
import {
  Box,
  Container,
  Flex,
  Image,
  Link,
  Text
} from '@chakra-ui/react';
import CryticalLogoImage from "../../assets/images/CRYTICAL_Logo_Icon.png";
import CryticalTextImage from "../../assets/images/CRYTICAL_Logo_Text.png";
import { TwitterOutlined, MediumOutlined, GithubOutlined } from '@ant-design/icons';
import SideBarLink from '../SideBarLink/SideBarLink';
import SideBarDetail from '../SideBarDetail/SideBarDetail';
import { Icon } from '@iconify/react';
import DiscordIcon from "../../assets/icons/discord.png";
import GitBookIcon from "../../assets/icons/gitbook.png";
import MediumIcon from "../../assets/icons/medium.png";
import TelegramIcon from "../../assets/icons/telegram.png";
import TwitterIcon from "../../assets/icons/twitter.png";
const SideBar = ({ active }) => {
  const [links, setLinks] = useState({
    before: [],
    current: [],
    after: []
  });
  const componentDidMount = () => {
    if (active === null || active === undefined) return;
    const linksData = [{
      index: "1",
      label: "Home",
      href: "/"
    }, {
      index: "2",
      label: "Pro",
      href: "/pro",
    }, {
      index: "3",
      label: "Claim Rewards",
      href: "/claimRewards",
    }, {
      index: "4",
      label: "Leaderboard",
      href: "/leaderBoard"
    }, {
      index: "5",
      disabled: true,
      label: "Launch Pad",
      href: "/launchPad",
    }, {
      index: "6",
      label: "NFT",
      href: "/nft"
    }, {
      index: "7",
      disabled: true,
      label: 'Bonds',
      href: "/bonds",
    }];
    let activeLinkFound = false;
    let before = [];
    let current = [];
    let after = [];
    for (let i = 0; i < linksData.length; ++i) {
      if (linksData[i].index === active) {
        activeLinkFound = true;
        current.push(linksData[i]);
      } else if (activeLinkFound === true) {
        after.push(linksData[i]);
      } else {
        before.push(linksData[i]);
      }
    }
    setLinks({
      before: before,
      current: current,
      after: after
    });
  }
  useEffect(componentDidMount, [active]);
  return (
    <Flex
      display={{ base: 'none', md: 'flex' }}
      flexDirection="column"

      width="247px"
      borderRadius="0px 20px 20px 0px"
      overflow="auto"
      overflowX="hidden"
      position="fixed"
      top="0px"
      maxHeight="100%">
      <Flex
        direction={"column"}
        background=" transparent 0% 0% no-repeat padding-box"
        boxShadow="inset 0px 6px 6px #0000002F"
        border="1px solid #FFFFFF26"
        borderRadius="0px 2px 20px 0px"
        backdropFilter="brightness(.96)"
      >
        <Box width="247px" height="200px" >
          <Image width="167px" fit="contain" margin="auto" marginTop="20px" src={CryticalLogoImage} />
        </Box>
        {links.before.map(
          (tx) =>
            <SideBarLink
              isdisabled={tx?.disabled}
              href={tx.href}
              text={tx.label}
            />
        )}
      </Flex>
      <Flex
        direction="column"
      >
        {links.current.map(
          (tx) =>
            <SideBarLink
              isdisabled={tx?.disabled}
              active={true}
              href={tx.href}
              text={tx.label}
            />
        )}
      </Flex>
      <Flex
        background="transparent 0% 0% no-repeat padding-box"
        boxShadow="inset 0px 6px 6px #0000002F"
        border="1px solid #FFFFFF26"
        borderRadius="0px 20px 20px 0px"
        backdropFilter="brightness(.96)"
        flexDirection="column" width="100%">
        {links.after.map(
          (tx) =>
            <SideBarLink
              isdisabled={tx?.disabled}
              href={tx.href}
              text={tx.label}
            />
        )}
        <Flex overflow="auto" flexDirection="column">
          <SideBarDetail
            title="Token Price"
            left_title="$0.890"
            right_title="2.50%"
          />
          <SideBarDetail
            title="AAS Countdown"
            left_title="21 : 30 : 25"
          />
          <SideBarDetail
            title="Yesterday’s AAS"
            left_title="3%"
            right_title="2.50%"
          />
          <SideBarDetail
            title="Token Balance"
            left_title="0.00000000"
          />
          <SideBarDetail
            title="Staked Balance"
            left_title="0.00000000"
          />
          <SideBarDetail
            title="Unclaimed Balance"
            left_title="0.00000000"
          />
        </Flex>
        <Flex width="100%" height="50px" flexDirection="row" justifyContent="space-around">
          <Image width="24px" fit="contain" src={TwitterIcon} />
          <Image width="24px" fit="contain" src={TelegramIcon} />
          <Image width="24px" fit="contain" src={MediumIcon} />
          <Image width="24px" fit="contain" src={GitBookIcon} />
          <Image width="24px" fit="contain" src={DiscordIcon} />
        </Flex>
      </Flex>
    </Flex>





  );
}

SideBar.propTypes = {};

SideBar.defaultProps = {};

export default SideBar;
