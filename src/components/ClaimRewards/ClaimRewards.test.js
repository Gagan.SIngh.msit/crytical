import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import ClaimRewards from './ClaimRewards';

describe('<ClaimRewards />', () => {
  test('it should mount', () => {
    render(<ClaimRewards />);
    
    const claimRewards = screen.getByTestId('ClaimRewards');

    expect(claimRewards).toBeInTheDocument();
  });
});