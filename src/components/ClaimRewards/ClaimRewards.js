import React from 'react';
import PropTypes from 'prop-types';
import styles from './ClaimRewards.module.scss';
import { Box, Button, Flex, Text } from '@chakra-ui/react';
import SideBarLink from '../SideBarLink/SideBarLink';
import SideBar from '../SideBar/SideBar';
import HeaderOptions from '../HeaderOptions/HeaderOptions';
import ClaimRewardsInfoBox from '../ClaimRewardsInfoBox/ClaimRewardsInfoBox';
import MobileHeader from '../MobileHeader/MobileHeader';
import CryticalBackground from '../../components/CryticalBackground/CryticalBackground';
const ClaimRewards = () => {
  const data =
  {
    top: {
      title: "Total Rewards",
      value: "# Tokens EARNED",
      subtitle: "Your Staking AAS Rewards are distributed once every 7 days."
    },
    bottom: {
      top: "Staking AAS Rewards",
    }
  };
  const data1 = {
    top: {
      title: "Current Market Value",
      value: "$",
      subtitle: "To particiapte in Quota’s membership exclusive on-chain referral program, mint your Quota Membership NFT and start sharing your referral code!"
    },
    bottom: {
      top: "Referral Rewards",
    }
  };
  return (
    <>
      <CryticalBackground />
      <Box maxWidth="100vw" w="100vw" h="100vh" overflow="auto" >
        <Flex direction="column" >
          <MobileHeader />
          <Flex color='white' height="100%">
            <SideBar active="3" />
            <Flex
              paddingLeft={{ base: "0px", md: "247px" }}
              flex="1"
              overflow="auto"
              flexDirection="column">
              <Flex flexDirection="column" >
                <HeaderOptions />
                <Box width="100%" height="43px">
                  <Text
                    fontStyle="var(--unnamed-font-style-normal) normal var(--unnamed-font-weight-medium) var(--unnamed-font-size-30)/var(--unnamed-line-spacing-46) var(--unnamed-font-family-poppins)"
                    fontSize={{ base: "16px", md: "30px" }}
                    paddingLeft={{ base: "20px", md: "58px" }}
                    color="var(--unnamed-color-565555)"
                    fontWeight="600">Claim Rewards</Text>
                </Box>
                <Flex
                  direction="column"
                  width="100%"
                  flex="1"
                  padding={{ base: "60px 20px", lg: "60px 58px" }}
                  gap="60px"
                >
                  <Flex
                    display={{ base: "flex", lg: "none" }}
                    flexDirection={{ base: "column", md: "row" }}
                    background="transparent 0% 0% no-repeat padding-box"
                    boxShadow="inset 0px 3px 6px #00000029"
                    border="1px solid #FFFFFF26"
                    borderRadius="20px"
                    backdropFilter="brightness(.96)"
                    height={{ base: "130px", md: "100px" }}
                    width="100%"
                    justifyContent="space-between"
                    alignItems="center"
                    padding={{ base: "20px", md: "0px 40px" }}
                    marginTop={{ md: "60px" }}>
                    <Text
                      color="#565555"
                      fontFamily="'Poppins', sans-serif"
                      letterSpacing="0.4px"
                      fontWeight="500"
                      paddingBottom={{ base: "13px", md: "0px" }}
                      textAlign={{ base: "center", md: "center" }}
                      fontSize={{ base: "10px", md: "16px" }}>Invite your friends to participate to earn more rewards!</Text>
                    <Button
                      background=" transparent linear-gradient(80deg, #FF7495 0%, #A0DF86 100%) 0% 0% no-repeat padding-box"
                      borderRadius="11px"
                      color="#565555"
                      letterSpacing="0px"
                      fontFamily="'Poppins', sans-serif"
                      fontSize={{ base: "11px", md: "16px" }}
                      fontWeight="600"
                      width={{ base: "237px", md: "310px" }}
                      height="55px"
                    >Invite your friends</Button>
                  </Flex>
                  <Flex
                    flexDirection={{ base: "column", lg: "row" }}
                    background="transparent 0% 0% no-repeat padding-box"
                    boxShadow="inset 0px 3px 6px #00000029"
                    border="1px solid #FFFFFF26"
                    borderRadius="20px"
                    backdropFilter="brightness(.96)"
                    height={{ base: "110px", lg: "100px" }}
                    width="100%"
                    justifyContent="space-between"
                    alignItems="center"
                    padding={{ base: "20px", lg: "0px 40px" }}
                    gap={{ base: "0px", lg: "265px" }}
                    fontWeight="600">
                    <Flex flex="1"
                      justifyContent="space-between"
                      width={{ base: "100%", md: "auto" }}>
                      <Text
                        color="#565555"
                        fontFamily="'Poppins', sans-serif"
                        letterSpacing="0.4px"
                        paddingBottom={{ base: "13px", md: "0px" }}
                        textAlign={{ base: "center", md: "center" }}
                        fontSize={{ base: "10px", md: "16px" }}>Total Rewards Collected :</Text>
                      <Text
                        display="flex"
                        color="#565555"
                        fontFamily="'Poppins', sans-serif"
                        letterSpacing="0.4px"
                        paddingBottom={{ base: "13px", md: "0px" }}
                        textAlign={{ base: "center", md: "center" }}
                        fontSize={{ base: "10px", md: "16px" }}>
                        <Text color="#FF82A0">10</Text> QEX</Text>

                    </Flex>
                    <Flex
                      flex="1"
                      justifyContent="space-between"
                      width={{ base: "100%", md: "auto" }}>
                      <Text
                        color="#565555"
                        fontFamily="'Poppins', sans-serif"
                        letterSpacing="0.4px"
                        paddingBottom={{ base: "13px", md: "0px" }}
                        textAlign={{ base: "center", md: "center" }}
                        fontSize={{ base: "10px", md: "16px" }}>Current Market Value :</Text>
                      <Text
                        color="#565555"
                        fontFamily="'Poppins', sans-serif"
                        letterSpacing="0.4px"
                        paddingBottom={{ base: "13px", md: "0px" }}
                        textAlign={{ base: "center", md: "center" }}
                        fontSize={{ base: "10px", md: "16px" }}>$ 20,000</Text>

                    </Flex>
                  </Flex>
                  <Flex
                    direction={{ base: "column", lg: "row" }}
                    justifyContent="space-between"
                    overflow={"auto"}
                    gap="58px"
                  >
                    <ClaimRewardsInfoBox
                      data={
                        data
                      } />
                    <ClaimRewardsInfoBox data={
                      data1
                    } />
                  </Flex>
                  <Flex
                    display={{ base: "none", lg: "flex" }}
                    flexDirection={{ base: "column", md: "row" }}
                    background="transparent 0% 0% no-repeat padding-box"
                    boxShadow="inset 0px 3px 6px #00000029"
                    border="1px solid #FFFFFF26"
                    borderRadius="20px"
                    backdropFilter="brightness(.96)"
                    height={{ base: "130px", md: "100px" }}
                    width="100%"
                    justifyContent="space-between"
                    alignItems="center"
                    padding={{ base: "20px", md: "0px 40px" }}
                    marginTop={{ md: "60px" }}>
                    <Text
                      color="#565555"
                      fontFamily="'Poppins', sans-serif"
                      letterSpacing="0.4px"
                      fontWeight="500"
                      paddingBottom={{ base: "13px", md: "0px" }}
                      textAlign={{ base: "center", md: "center" }}
                      fontSize={{ base: "10px", md: "16px" }}>Invite your friends to participate to earn more rewards!</Text>
                    <Button
                      background=" transparent linear-gradient(80deg, #FF7495 0%, #A0DF86 100%) 0% 0% no-repeat padding-box"
                      borderRadius="11px"
                      color="#565555"
                      letterSpacing="0px"
                      fontFamily="'Poppins', sans-serif"
                      fontSize={{ base: "11px", md: "16px" }}
                      fontWeight="600"
                      width={{ base: "237px", md: "310px" }}
                      height="55px"
                    >Invite your friends</Button>
                  </Flex>
                </Flex>
              </Flex>
            </Flex>
          </Flex>
        </Flex>

      </Box >
    </>

  );
}

ClaimRewards.propTypes = {};

ClaimRewards.defaultProps = {};

export default ClaimRewards;
