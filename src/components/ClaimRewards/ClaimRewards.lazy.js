import React, { lazy, Suspense } from 'react';

const LazyClaimRewards = lazy(() => import('./ClaimRewards'));

const ClaimRewards = props => (
  <Suspense fallback={null}>
    <LazyClaimRewards {...props} />
  </Suspense>
);

export default ClaimRewards;
