import React from 'react';
import styles from './CryticalGrid.module.scss';
import { SimpleGrid, Image, Flex, Text } from '@chakra-ui/react';
import CryticalBlack from "../../assets/images/CRYTICAL_NFT_Black.png";

const CryticalGrid = () => {
  const arr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15];
  return (
    <div className={styles.CryticalGrid} data-testid="CryticalGrid">
      <SimpleGrid columns={{ base: 3, lg: 5 }} spacing={10}>
        {arr.map((tx) => <Flex direction="column">
          <Image height="266px" fit="contain" src={CryticalBlack} />
          <Text
            color="#565555"
            textAlign="center">
            0X…4eF78</Text>
        </Flex>)}
      </SimpleGrid>
    </div>
  );
}

CryticalGrid.propTypes = {};

CryticalGrid.defaultProps = {};

export default CryticalGrid;
