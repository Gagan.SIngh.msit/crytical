import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import CryticalGrid from './CryticalGrid';

describe('<CryticalGrid />', () => {
  test('it should mount', () => {
    render(<CryticalGrid />);
    
    const cryticalGrid = screen.getByTestId('CryticalGrid');

    expect(cryticalGrid).toBeInTheDocument();
  });
});