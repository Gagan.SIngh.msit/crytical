import React, { lazy, Suspense } from 'react';

const LazyCryticalGrid = lazy(() => import('./CryticalGrid'));

const CryticalGrid = props => (
  <Suspense fallback={null}>
    <LazyCryticalGrid {...props} />
  </Suspense>
);

export default CryticalGrid;
