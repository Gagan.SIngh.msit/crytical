import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import BondsInfoTextBox from './BondsInfoTextBox';

describe('<BondsInfoTextBox />', () => {
  test('it should mount', () => {
    render(<BondsInfoTextBox />);
    
    const bondsInfoTextBox = screen.getByTestId('BondsInfoTextBox');

    expect(bondsInfoTextBox).toBeInTheDocument();
  });
});