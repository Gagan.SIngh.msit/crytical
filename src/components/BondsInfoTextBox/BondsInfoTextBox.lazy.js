import React, { lazy, Suspense } from 'react';

const LazyBondsInfoTextBox = lazy(() => import('./BondsInfoTextBox'));

const BondsInfoTextBox = props => (
  <Suspense fallback={null}>
    <LazyBondsInfoTextBox {...props} />
  </Suspense>
);

export default BondsInfoTextBox;
