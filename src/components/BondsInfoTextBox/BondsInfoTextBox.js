import React from 'react';
import { Flex, Text } from '@chakra-ui/react';
import { TriangleDownIcon, } from '@chakra-ui/icons';
const BondsInfoTextBox = ({
  title,
  subtitle,
  caption,
  icon
}) => {
  return (
    <Flex direction="column" color="#565555" overflow="hidden"
    >
      <Text
        fontSize={{ base: "16px", md: "20px" }}
        fontFamily="600px"
        marginBottom="20px"
        fontWeight="500">{title}</Text>
      {
        icon === true ?
          <Text
            color="#EF0009"
            fontSize={{ base: "24px", md: "30px" }}
            fontWeight="800"
            alignContent="center">
            <TriangleDownIcon
              fontSize="16px" /> {subtitle}</Text>
          :
          <Text
            color="#FF7495"
            fontSize={{ base: "24px", md: "30px" }}
            fontWeight="600">{subtitle}</Text>
      }

      <Text
        position={{ base: "relative" }}
        left={{ base: "60px", md: "0px" }}
        top={{ base: "-23px", md: "0px" }}
        fontSize={{ base: "12px", md: "16px" }}>{caption}</Text>
    </Flex>
  );
}

BondsInfoTextBox.propTypes = {};

BondsInfoTextBox.defaultProps = {};

export default BondsInfoTextBox;
