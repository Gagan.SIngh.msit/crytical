import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import CryticalAreaChart from './CryticalAreaChart';

describe('<CryticalAreaChart />', () => {
  test('it should mount', () => {
    render(<CryticalAreaChart />);
    
    const cryticalAreaChart = screen.getByTestId('CryticalAreaChart');

    expect(cryticalAreaChart).toBeInTheDocument();
  });
});