import React, { useRef, useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import styles from './CryticalAreaChart.module.scss';
import { Flex, Button, Text, Box } from '@chakra-ui/react';
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Filler,
  Legend,
} from 'chart.js';
import { Line } from 'react-chartjs-2';
import {
  Popover,
  PopoverTrigger,
  PopoverContent,
  PopoverHeader,
  PopoverBody,
  PopoverFooter,
  PopoverArrow,
  PopoverCloseButton,
  PopoverAnchor,
} from '@chakra-ui/react'
import { InfoOutlineIcon } from '@chakra-ui/icons';
import { InfoCircleOutlined, InfoOutlined } from '@ant-design/icons';
ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Filler,
  Legend
);

const CryticalAreaChart = ({
  title,
  isFill,
  labels,
  values,
  isGradient,
  subtitle
}) => {
  const chartRef = useRef(null);
  const data = {
    labels,
    datasets: [
      {
        fill: isFill != null && isFill != undefined ? isFill : true,
        data: values,
        lineTension: 0.6,
        borderColor: isGradient === true ? '#A8E190' : "#A2A2A2",

      },
    ],
    lineTension: 0.8
  };
  const options = {
    responsive: true,
    tooltips: {
      callbacks: {
        label: function (tooltipItem) {
          console.log(tooltipItem);
          return "$" + Number(tooltipItem.yLabel) + " and so worth it !";
        }
      }
    },
    plugins: {
      legend: {
        position: 'top',
      },
      title: {
        display: true,
        text: '',
      },
    },
    bezierCurve: true
  };

  const [chartData, setChartData] = useState({
    datasets: [],
  });

  function createGradient(ctx, area) {
    const colorStart = '#F5C3D08D';
    const colorMid = '#FFFFFF00';

    const gradient = ctx.createLinearGradient(0, area.bottom, 0, area.top);
    gradient.addColorStop(0, colorMid);
    gradient.addColorStop(0.1, colorStart);


    return gradient;
  }



  useEffect(() => {
    const chart = chartRef.current;

    if (!chart) {
      return;
    }

    const chartData = {
      ...data,
      datasets: data.datasets.map(dataset => ({
        ...dataset,
        backgroundColor: createGradient(chart.ctx, chart.chartArea),
      })),
    };

    setChartData(chartData);
  }, []);
  return (
    <Flex direction="column" width="100%" height="100%" overflow="auto">
      <Flex justifyContent="space-between" alignItems="center">
        <Flex>
          <Text
            fontStyle="var(--unnamed-font-style-normal) normal var(--unnamed-font-weight-medium) var(--unnamed-font-size-30)/var(--unnamed-line-spacing-46) var(--unnamed-font-family-poppins)"
            fontSize={{ base: "10px", md: "22px" }}
            color="var(--unnamed-color-565555)"
            fontWeight="bold"
            paddingRight="10px">{title}</Text>
          {subtitle !== null && subtitle !== undefined &&
            <Popover >
              <PopoverAnchor>
                <PopoverTrigger>
                  <InfoCircleOutlined style={{ color: "#565555" }} />
                </PopoverTrigger>
              </PopoverAnchor>
              <PopoverContent color="#333333" fontSize="12px" borderRadius="10px">

                <PopoverBody
                  padding="22px 21px 21.17px 20.83px">{subtitle} </PopoverBody>
              </PopoverContent>
            </Popover>

          }
        </Flex>
        <Box>
          <button
            className={styles.CryticalAreaChart__ButtonStyle}
            boxShadow="inset -2px -2px 3px #FFFFFF6A">
            7D
          </button>
          <button
            className={styles.CryticalAreaChart__ButtonStyle}
            boxShadow="2px -2px 3px #FFFFFF65">
            30D
          </button>
          <button
            className={styles.CryticalAreaChart__ButtonStyle}
            boxShadow="-2px -2px 3px #FFFFFF65"
          >
            All
          </button>
        </Box>
      </Flex>
      <Flex

        pos="relative"
        width={{ base: "100%", md: "100%" }}>
        <Line ref={chartRef} options={options} data={chartData} />
      </Flex>
    </Flex>
  );
}

CryticalAreaChart.propTypes = {};

CryticalAreaChart.defaultProps = {};

export default CryticalAreaChart;
