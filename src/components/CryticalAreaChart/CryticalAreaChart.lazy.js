import React, { lazy, Suspense } from 'react';

const LazyCryticalAreaChart = lazy(() => import('./CryticalAreaChart'));

const CryticalAreaChart = props => (
  <Suspense fallback={null}>
    <LazyCryticalAreaChart {...props} />
  </Suspense>
);

export default CryticalAreaChart;
