import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import ClaimRewardsInfoBox from './ClaimRewardsInfoBox';

describe('<ClaimRewardsInfoBox />', () => {
  test('it should mount', () => {
    render(<ClaimRewardsInfoBox />);
    
    const claimRewardsInfoBox = screen.getByTestId('ClaimRewardsInfoBox');

    expect(claimRewardsInfoBox).toBeInTheDocument();
  });
});