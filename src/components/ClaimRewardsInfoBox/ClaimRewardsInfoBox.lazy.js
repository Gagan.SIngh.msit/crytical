import React, { lazy, Suspense } from 'react';

const LazyClaimRewardsInfoBox = lazy(() => import('./ClaimRewardsInfoBox'));

const ClaimRewardsInfoBox = props => (
  <Suspense fallback={null}>
    <LazyClaimRewardsInfoBox {...props} />
  </Suspense>
);

export default ClaimRewardsInfoBox;
