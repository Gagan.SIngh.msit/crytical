import React from 'react';
import PropTypes from 'prop-types';
import styles from './ClaimRewardsInfoBox.module.scss';
import { Flex, Text, Button, Box } from '@chakra-ui/react';
import {
  Popover,
  PopoverTrigger,
  PopoverContent,
  PopoverHeader,
  PopoverBody,
  PopoverFooter,
  PopoverArrow,
  PopoverCloseButton,
  PopoverAnchor,
} from '@chakra-ui/react';

import { InfoCircleOutlined, InfoOutlined } from '@ant-design/icons';

const ClaimRewardsInfoBox = ({ data }) => {
  return (
    <Flex
      direction="column"
      flex="1"
      gap={{ base: "20px", md: "37px" }}
      justifyContent="space-between"
      fontFamily="'Poppins', sans-serif">

      <Flex
        direction="column"
        alignItems="center"
        background="transparent 0% 0% no-repeat padding-box"
        boxShadow="inset 0px 3px 6px #00000029"
        border="1px solid #FFFFFF26"
        borderRadius="20px"
        backdropFilter="brightness(.96)"
        width={{ base: "100%", md: "100%" }}
        height={{ base: "230px", md: "434px" }}
        justifyContent="space-between"
        padding={{ base: "20px", md: "50px" }}
      >
        <Flex
          gap="10px"
        >
          <Text
            color="#565555"
            letterSpacing="0.5px"
            fontSize="22px"
            fontWeight="500"
            textAlign="center"
          >
            {data?.bottom?.top}
          </Text>
          <Popover >
            <PopoverAnchor>
              <PopoverTrigger>
                <Box
                  border="2px solid #000000"
                  borderRadius="4px"
                  width="15px"
                  height="16px">
                  <InfoOutlined style={{
                    position: "relative",
                    top: "-7px",
                    color: "#000",
                    fontSize: "11px"
                  }} />
                </Box>
              </PopoverTrigger>
            </PopoverAnchor>
            <PopoverContent color="#333333" fontSize="12px" borderRadius="10px">
              <PopoverBody
                padding="22px 21px 21.17px 20.83px">
                {data?.top?.subtitle} </PopoverBody>
            </PopoverContent>
          </Popover>
        </Flex>
        <Flex
          direction="column"
          color="#565555"
          textAlign="center">
          <Text fontSize={{ base: "13px", md: "16px" }}>00 : 00 : 00</Text>
          <Text fontSize={{ base: "20px", md: "35px" }} display="flex" fontWeight="500">
            <Text color="#FF82A0">100</Text> <Text marginLeft="10px">4.0</Text></Text>
          <Text fontSize={{ base: "14px", md: "16px" }}>$$$ Market Value</Text>
        </Flex>

        <Button
          background="transparent linear-gradient(80deg, #FF7495 0%, #A0DF86 100%) 0% 0% no-repeat padding-box"
          borderRadius="11px"
          width={{ base: "100%", xl: "310px" }}
          color="#565555"
          fontWeight="600">Claim Rewards</Button>
      </Flex>
    </Flex >
  );
}

ClaimRewardsInfoBox.propTypes = {};

ClaimRewardsInfoBox.defaultProps = {};

export default ClaimRewardsInfoBox;
