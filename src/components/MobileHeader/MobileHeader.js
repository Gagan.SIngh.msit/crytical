import React from 'react';
import styles from './MobileHeader.module.scss';
import { Flex, Image, useDisclosure, Button, Link, Text, Select } from '@chakra-ui/react';
import CryticalLogo from "../../assets/images/QUOTA_Logo_2.png";
import { HamburgerIcon } from '@chakra-ui/icons';
import { WalletOutlined } from '@ant-design/icons';
import { Icon } from '@iconify/react';
import { TwitterOutlined, MediumOutlined, GithubOutlined } from '@ant-design/icons';
import {
  Drawer,
  DrawerBody,
  DrawerFooter,
  DrawerHeader,
  DrawerOverlay,
  DrawerContent,
  DrawerCloseButton,
} from '@chakra-ui/react';
import { Link as RouterLink } from 'react-router-dom';

const MobileHeader = ({ active }) => {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const btnRef = React.useRef();
  const checkActive = (currentType) => {
    return active === currentType;
  }
  return (
    <>
      <Flex
        display={{ base: "flex", md: "none" }}
        background="transparent 0% 0% no-repeat padding-box"
        boxShadow="inset 0px 6px 6px #0000002F"
        backdropFilter="brightness(.96)"
        height="70px"
        padding="15px 25px"
        width="100%"
        justifyContent="space-between"
        alignItems="center"
        marginBottom="20px">
        <Image height="100%" fit="contain" src={CryticalLogo} />
        <HamburgerIcon fontSize="28px" color="#4B434B" onClick={onOpen} />
      </Flex>
      <Drawer

        isOpen={isOpen}
        placement='right'
        onClose={onClose}
        finalFocusRef={btnRef}
      >
        <DrawerOverlay />
        <DrawerContent background="url('/img/CRYTICAL_Mobile_Background.png') 0% 0% no-repeat padding-box">
          <DrawerCloseButton />
          <DrawerHeader></DrawerHeader>

          <DrawerBody paddingTop="50px">
            <Flex direction="column" width="100%" alignItems="end">
              <Link
                className={styles.MobileHeader__Link}
                active={checkActive("Home")}
                as={RouterLink}
                to="/"
              >Home</Link>
              <Link
                className={styles.MobileHeader__Link}
                active={checkActive("Pro")}
                as={RouterLink}
                to="/pro"
              >Pro</Link>
              <Link
                className={styles.MobileHeader__Link}
                active={checkActive("Claim Rewards")}
                as={RouterLink}
                to="/claimRewards"
              >Claim Reward</Link>
              <Link
                className={styles.MobileHeader__Link}
                active={checkActive("LeaderBoard")}
                as={RouterLink}
                to="/leaderBoard"
              >Leaderboard</Link>
              <Link
                className={styles.MobileHeader__Link}
                active={checkActive("Launch Pad")}
                as={RouterLink}
                to="/launchPad"
              >Launch Pad</Link>
              <Link
                className={styles.MobileHeader__Link}
                active={checkActive("NFT")}
                as={RouterLink}
                to="/nft"
              >NFT</Link>
              <Link className={styles.MobileHeader__Link}
                active={checkActive("Bonds")}
                as={RouterLink}
                to="/bonds"
              >Bonds</Link>
              <Flex direction="row" marginTop="47px" justifyContent="center" width="100%">
                <Button
                  background="#E9E6E7 0% 0% no-repeat padding-box"
                  width="199px"
                  height="45px"
                  boxShadow="-2px -2px 3px #FFFFFF65"
                  borderRadius="4px"><WalletOutlined
                    style={{ color: '#231F20' }} />
                  <Text
                    paddingLeft="10px"
                    fontStyle="var(--unnamed-font-style-normal) normal var(--unnamed-font-weight-normal) 16px/25px var(--unnamed-font-family-poppins)"
                    color="#231F20"
                    fontFamily="'Poppins', sans-serif"
                  >Connect Wallet
                  </Text></Button>
              </Flex>
              {/* <Select
                fontFamily="'Poppins', sans-serif"
                color="#231F20"
                placeholder='ENG'
                width="100px"
                height="20px"
                border="none"
                value="ENG"
                fontWeight="600"
                marginTop="47px"
              >
                <option
                  defaultChecked={true}
                  value="ENG"
                >
                  ENG</option>
                <option value="KOR">KOR</option>
              </Select> */}
              <Flex marginTop="47px" width="100%" height="50px" style={{ fontSize: '22px', color: '#4B434B' }} flexDirection="row" justifyContent="space-around">
                <Flex width="50px" justifyContent="center" height="50px">
                  <TwitterOutlined />
                </Flex>
                <Flex width="50px" justifyContent="center" height="50px">
                  <Icon icon="teenyicons:telegram-outline" />
                </Flex>
                <Flex width="50px" justifyContent="center" height="50px">
                  <MediumOutlined />
                </Flex>
                <Flex width="50px" justifyContent="center" height="50px">
                  <GithubOutlined />
                </Flex>
              </Flex>
            </Flex>
          </DrawerBody>

          <DrawerFooter>
          </DrawerFooter>
        </DrawerContent>
      </Drawer>
    </>
  );
}

MobileHeader.propTypes = {};

MobileHeader.defaultProps = {};

export default MobileHeader;
