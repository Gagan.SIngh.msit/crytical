import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import MobileHeader from './MobileHeader';

describe('<MobileHeader />', () => {
  test('it should mount', () => {
    render(<MobileHeader />);
    
    const mobileHeader = screen.getByTestId('MobileHeader');

    expect(mobileHeader).toBeInTheDocument();
  });
});