import React, { lazy, Suspense } from 'react';

const LazyMobileHeader = lazy(() => import('./MobileHeader'));

const MobileHeader = props => (
  <Suspense fallback={null}>
    <LazyMobileHeader {...props} />
  </Suspense>
);

export default MobileHeader;
