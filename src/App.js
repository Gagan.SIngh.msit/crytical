import logo from './logo.svg';
import { Box, ChakraProvider } from '@chakra-ui/react'
import './App.css';
import {
  BrowserRouter,
  Routes,
  Route,
} from "react-router-dom";
import 'antd/dist/antd.css';
import Home from './pages/Home/Home';
import Pro from './pages/Pro/Pro';
import Nft from './pages/NFT/NFT';
import ClaimRewards from './components/ClaimRewards/ClaimRewards';
import LeaderBoard from './pages/LeaderBoard/LeaderBoard';
import Bonds from './pages/Bonds/Bonds';
import LaunchPad from './pages/LaunchPad/LaunchPad';
import { extendTheme } from "@chakra-ui/react"
import BidDetailPage from './pages/BidDetailPage/BidDetailPage';
import CreatePool from './pages/CreatePool/CreatePool';

const theme = extendTheme({
  colors: {
    progressPink: {
      500: '#FF7495'
    },
    progressGreen: {
      500: '#A0DF86'
    },
    progressBlue: {
      500: '#155AC1'
    },
    radioBlack: {
      500: '#4B434B'
    }
  },
});

function App() {
  return (
    <ChakraProvider theme={theme}>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/pro" element={<Pro />} />
          <Route path="/claimRewards" element={<ClaimRewards />} />
          <Route path="/leaderBoard" element={<LeaderBoard />} />
          <Route path="/nft" element={<Nft />} />
          <Route path="/bonds" element={<Bonds />} />
          <Route path="/launchPad" element={<LaunchPad />} />
          <Route path="/bidDetail" element={<BidDetailPage />} />
          <Route path="/createPool" element={<CreatePool />} />
        </Routes>
      </BrowserRouter>
    </ChakraProvider>

  );
}

export default App;
