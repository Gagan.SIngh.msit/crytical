import React, { useState } from 'react';
import { Radio, RadioGroup, Flex, Text, Box, Divider, Input, InputGroup, Stack, Button, InputRightElement } from '@chakra-ui/react';
import SideBar from '../../components/SideBar/SideBar';
import HeaderOptions from '../../components/HeaderOptions/HeaderOptions';
import { CloseIcon, ViewIcon, ViewOffIcon } from '@chakra-ui/icons';
import MobileHeader from '../../components/MobileHeader/MobileHeader';
import CryticalBackground from '../../components/CryticalBackground/CryticalBackground';

const CreatePool = () => {
  const [isAllocationVisible, setIsAllocationVisible] = useState(false);
  const [isPasswordVisible, setIsPasswordVisible] = useState(false);
  const [show, setShow] = React.useState(false)
  const handleClick = () => setShow(!show)
  const changeLimits = (v) => {
    v === "ROSE" ? setIsAllocationVisible(true) : setIsAllocationVisible(false);
  }
  const togglePassword = (v) => {
    v === "Private" ? setIsPasswordVisible(true) : setIsPasswordVisible(false);
  }
  return (
    <>
      <CryticalBackground />

      <Box maxWidth="100vw" w="100vw" h="100vh" overflow="auto" >
        <Flex direction="column">
          <MobileHeader />
          <Flex color='white' height="100%">
            <SideBar active="Home" />
            <Flex paddingLeft={{ base: "0px", md: "247px" }}
              padding={{ base: "20px", md: "0px 0px 0px 247px" }} flex="1" overflow="auto" flexDirection="column" height="auto" >
              <Flex flexDirection="column">
                <HeaderOptions />
              </Flex>
              <Flex
                direction="column"
                flex="1"
                margin={{ base: "0px", md: "61px" }}
                background="transparent linear-gradient(237deg, #EBE5E5 0%, #E3DDE1 86%, #EAE4E4 100%) 0% 0% no-repeat padding-box"
                boxShadow="3px 3px 6px #00000029"
                borderRadius="20px"
                padding={{ base: "20px", md: "45px" }}
                marginBottom="60px"
                color="#242B45"
                fontWeight="600"
                fontSize="16px"
              >
                <Flex
                  direction="row"
                  color="#242B45"
                  alignItems="center"
                  fontWeight="600"
                  gap="5px"
                  fontSize="14px">
                  <CloseIcon marginLeft="auto" fontSize="16px" fontWeight="800" />
                </Flex>
                <Text fontSize={{ base: "10px", md: "12px" }}>INITIAL TOKEN OFFERING</Text>
                <Text
                  marginTop="32px"
                  fontSize={{ base: "16px", md: "26px" }}
                >Create a Fixed-swap Pool</Text>
                <Divider direction="horizontal" marginTop="12px" height="2px" background="#D6D6D6" border="1px solid #D6D6D6" />
                <Text
                  fontSize={{ base: "14px", md: "24px" }}
                  textDecoration="underline"
                  marginTop="43px">Pool Settings</Text>
                <Flex direction={{ base: "column", xl: "row" }} gap={{ base: "0px", md: "150px" }} fontSize={{ base: "12px", md: "14px" }}>
                  <Flex direction="column" width={{ base: "100%", xl: "50%" }}>

                    <Text
                      marginTop="32px" marginBottom="10px">Token Contract Address</Text>
                    <Input
                      background="transparent linear-gradient(95deg, #23009639 0%, #5D53534D 100%) 0% 0% no-repeat padding-box"
                      backdropFilter="blur(17px)"
                      border="1px solid #FFFFFF26"
                      borderRadius="10px"
                      height={{ base: "35px", md: "50px" }}
                    />
                    <Text marginTop="32px" marginBottom="10px">Swap Ratio</Text>
                    <Flex alignItems="center">
                      <Text display={{ base: "none", md: "inline-block" }} fontWeight="400" width="80px">1 ROSE =</Text>
                      <Text display={{ base: "inline-block", md: "none" }} fontWeight="400" width="20px">1 =</Text>

                      <Input background="transparent linear-gradient(95deg, #23009639 0%, #5D53534D 100%) 0% 0% no-repeat padding-box"
                        backdropFilter="blur(17px)"
                        border="1px solid #FFFFFF26"
                        borderRadius="10px"
                        height={{ base: "35px", md: "50px" }} />
                    </Flex>

                    <Flex alignItems="center" marginTop="32px" marginBottom="10px" justifyContent="space-between">
                      <Text>Total Sale Amount</Text>
                      <Flex alignItems="center">
                        <Text background="#FF7495 0% 0% no-repeat padding-box"
                          borderRadius="20px"
                          fontSize={{ base: "8px", md: "14px" }}
                          marginRight="10px"
                          padding="5px 17px 6px 17px">Max.</Text>
                        <Text fontSize={{ base: "8px", md: "14px" }}>Balance: --</Text>
                      </Flex>
                    </Flex>
                    <Input
                      background="transparent linear-gradient(95deg, #23009639 0%, #5D53534D 100%) 0% 0% no-repeat padding-box"
                      backdropFilter="blur(17px)"
                      border="1px solid #FFFFFF26"
                      borderRadius="10px"
                      height={{ base: "35px", md: "50px" }} />
                    <Text marginTop="32px" marginBottom="10px" >Pool Name</Text>
                    <Input
                      background="transparent linear-gradient(95deg, #23009639 0%, #5D53534D 100%) 0% 0% no-repeat padding-box"
                      backdropFilter="blur(17px)"
                      border="1px solid #FFFFFF26"
                      borderRadius="10px"
                      height={{ base: "35px", md: "50px" }} />
                  </Flex>
                  <Flex direction="column" flex="1">
                    <Text marginTop="32px" marginBottom="10px">Pool Running Time</Text>
                    <Flex gap="9px">
                      <InputGroup>
                        <Input
                          background="transparent linear-gradient(95deg, #23009639 0%, #5D53534D 100%) 0% 0% no-repeat padding-box"
                          backdropFilter="blur(17px)"
                          border="1px solid #FFFFFF26"
                          borderRadius="10px"
                          height={{ base: "35px", md: "50px" }}
                          paddingRight="50px" />
                        <InputRightElement children={
                          <Text
                            paddingRight="15px"
                            paddingTop={{ base: "0px", md: "10px" }}
                            fontWeight="400"
                            fontSize="12px"
                          >
                            Days
                          </Text>} />
                      </InputGroup>

                      <InputGroup>
                        <Input
                          background="transparent linear-gradient(95deg, #23009639 0%, #5D53534D 100%) 0% 0% no-repeat padding-box"
                          backdropFilter="blur(17px)"
                          border="1px solid #FFFFFF26"
                          borderRadius="10px"
                          height={{ base: "35px", md: "50px" }}
                          paddingRight="50px" />
                        <InputRightElement children={
                          <Text
                            paddingRight="15px"
                            fontWeight="400"
                            fontSize="12px"
                            paddingTop={{ base: "0px", md: "10px" }}
                          >
                            Hours
                          </Text>} />
                      </InputGroup>

                      <InputGroup>
                        <Input
                          background="transparent linear-gradient(95deg, #23009639 0%, #5D53534D 100%) 0% 0% no-repeat padding-box"
                          backdropFilter="blur(17px)"
                          border="1px solid #FFFFFF26"
                          borderRadius="10px"
                          height={{ base: "35px", md: "50px" }}
                          paddingRight="50px" />
                        <InputRightElement children={
                          <Text
                            paddingRight="15px"
                            paddingTop={{ base: "0px", md: "10px" }}
                            fontWeight="400"
                            fontSize="12px"
                          >
                            Minutes
                          </Text>} />
                      </InputGroup>
                    </Flex>
                    <Text fontSize={{ base: "14px", md: "22px" }} textDecoration="underline" marginTop="26px" marginBottom="14px">
                      Maximum Allocation per Wallet
                    </Text>
                    <RadioGroup onChange={changeLimits} >
                      <Stack direction='row'>
                        <Radio
                          colorScheme="radioBlack"
                          value='No Limits'
                          background="#FFFFFF 0% 0% no-repeat padding-box"
                          border="1px solid #4B434B"
                        ><Text
                          fontSize={{ base: "8px", md: "16px" }}>No Limits</Text></Radio>
                        <Radio
                          colorScheme="radioBlack"
                          value='ROSE'
                          background="#FFFFFF 0% 0% no-repeat padding-box"
                          border="1px solid #4B434B"><Text
                            fontSize={{ base: "8px", md: "16px" }}
                            display={{ base: "inline-block", md: "none" }}>4.0</Text>
                          <Text
                            fontSize={{ base: "8px", md: "16px" }}
                            display={{ base: "none", md: "inline-block" }}>ROSE</Text></Radio>
                      </Stack>
                    </RadioGroup>
                    <Box height="52px">
                      {
                        isAllocationVisible === true ? <Flex alignItems="center" marginTop="10px">
                          <Text fontWeight="400" width="90px">Allocation</Text>
                          <InputGroup>
                            <Input background="transparent linear-gradient(95deg, #23009639 0%, #5D53534D 100%) 0% 0% no-repeat padding-box"
                              backdropFilter="blur(17px)"
                              border="1px solid #FFFFFF26"
                              borderRadius="10px"
                              height={{ base: "35px", md: "50px" }}
                              paddingRight="50px" />
                            <InputRightElement
                              display={{ base: "none", md: "flex" }}
                              children={<Text paddingTop={{ base: "0px", md: "10px" }} paddingRight="15px" fontWeight="400" fontSize={{ base: "08px", md: "16px" }}>ROSE</Text>} />
                          </InputGroup>
                        </Flex> : null
                      }
                    </Box>
                    <Text fontSize="22px" textDecoration="underline" marginTop="27px" marginBottom="14px">
                      Participants
                    </Text>
                    <RadioGroup onChange={togglePassword} >
                      <Stack direction='row'>
                        <Radio
                          colorScheme="radioBlack"
                          value='ROSE holders'

                          background="#FFFFFF 0% 0% no-repeat padding-box"
                          border="2px solid #4B434B"><Text
                            fontSize={{ base: "8px", md: "16px" }}
                            display={{ base: "inline-block", md: "none" }}>4.0 holders</Text>
                          <Text
                            fontSize={{ base: "8px", md: "16px" }}
                            display={{ base: "none", md: "inline-block" }}>ROSE holders</Text></Radio>
                        <Radio
                          colorScheme="radioBlack"
                          value='Public'
                          background="#FFFFFF 0% 0% no-repeat padding-box"
                          border="1px solid #4B434B"
                        ><Text
                          fontSize={{ base: "8px", md: "16px" }}>Public</Text></Radio>
                        <Radio
                          colorScheme="radioBlack"
                          value='Private'
                          background="#FFFFFF 0% 0% no-repeat padding-box"
                          border="1px solid #4B434B"><Text
                            fontSize={{ base: "8px", md: "16px" }}>Private</Text> </Radio>
                      </Stack>
                    </RadioGroup>

                    <Box height="52px">
                      {
                        isPasswordVisible === true ? <Flex alignItems="center" marginTop="10px">
                          <Text fontWeight="400" width="90px">Password</Text>
                          <InputGroup>
                            <Input background="transparent linear-gradient(95deg, #23009639 0%, #5D53534D 100%) 0% 0% no-repeat padding-box"
                              backdropFilter="blur(17px)"
                              border="1px solid #FFFFFF26"
                              borderRadius="10px"
                              height={{ base: "35px", md: "50px" }}
                              paddingRight="50px"
                              type={show ? 'text' : 'password'}
                            />
                            <InputRightElement
                              children={
                                <Button
                                  position="relative"
                                  top={{ base: "-3px", md: "5px" }}
                                  height={{ base: "35px", md: "50px" }}
                                  background="transparent" fontSize={{ base: "15px", md: "22px" }} onClick={handleClick}>
                                  {show ? <ViewIcon /> : <ViewOffIcon />}
                                </Button>} />
                          </InputGroup>
                        </Flex> : null
                      }
                    </Box>
                    <Button
                      background="transparent linear-gradient(90deg, #FF9AE8 0%, #BDFD9A 100%) 0% 0% no-repeat padding-box"
                      borderRadius="11px"
                      marginTop="91px"
                      height="57px">Launch</Button>
                  </Flex>
                </Flex>
              </Flex>
            </Flex>
          </Flex>
        </Flex>

      </Box >
    </>
  );
}

CreatePool.propTypes = {};

CreatePool.defaultProps = {};

export default CreatePool;
