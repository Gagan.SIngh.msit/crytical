import React, { lazy, Suspense } from 'react';

const LazyCreatePool = lazy(() => import('./CreatePool'));

const CreatePool = props => (
  <Suspense fallback={null}>
    <LazyCreatePool {...props} />
  </Suspense>
);

export default CreatePool;
