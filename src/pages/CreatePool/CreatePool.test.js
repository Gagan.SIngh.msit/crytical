import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import CreatePool from './CreatePool';

describe('<CreatePool />', () => {
  test('it should mount', () => {
    render(<CreatePool />);
    
    const createPool = screen.getByTestId('CreatePool');

    expect(createPool).toBeInTheDocument();
  });
});