import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import BidDetailPage from './BidDetailPage';

describe('<BidDetailPage />', () => {
  test('it should mount', () => {
    render(<BidDetailPage />);
    
    const bidDetailPage = screen.getByTestId('BidDetailPage');

    expect(bidDetailPage).toBeInTheDocument();
  });
});