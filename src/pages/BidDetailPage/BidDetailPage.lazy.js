import React, { lazy, Suspense } from 'react';

const LazyBidDetailPage = lazy(() => import('./BidDetailPage'));

const BidDetailPage = props => (
  <Suspense fallback={null}>
    <LazyBidDetailPage {...props} />
  </Suspense>
);

export default BidDetailPage;
