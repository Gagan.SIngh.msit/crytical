import React from 'react';
import { Box, Flex, InputGroup, InputRightElement, Progress, Text, Input, Button } from '@chakra-ui/react';
import SideBar from '../../components/SideBar/SideBar';
import HeaderOptions from '../../components/HeaderOptions/HeaderOptions';
import { ChevronLeftIcon } from '@chakra-ui/icons';
import MobileHeader from '../../components/MobileHeader/MobileHeader';
import CryticalBackground from '../../components/CryticalBackground/CryticalBackground';
const BidDetailPage = () => (
  <>
    <CryticalBackground />
    <Box maxWidth="100vw" w="100vw" h="100vh" overflow="auto" >
      <Flex direction="column">
        <MobileHeader />
        <Flex color='white' height="100%">
          <SideBar active="Home" />
          <Flex paddingLeft={{ base: "0px", md: "247px" }}
            padding={{ base: "20px", md: "0px 0px 0px 247px" }} flex="1" overflow="auto" flexDirection="column" height="auto" >
            <Flex flexDirection="column">
              <HeaderOptions />

              <Box display={{ base: "block", md: "none" }} width="100%" height="43px">
                <Text
                  fontStyle="var(--unnamed-font-style-normal) normal var(--unnamed-font-weight-medium) var(--unnamed-font-size-30)/var(--unnamed-line-spacing-46) var(--unnamed-font-family-poppins)"
                  fontSize={{ base: "16px", md: "30px" }}
                  paddingLeft={{ base: "20px", md: "58px" }}
                  color="var(--unnamed-color-565555)"
                  fontWeight="600">Closed</Text>
              </Box>
            </Flex>
            <Flex
              direction="column"
              flex="1"
              margin={{ base: "0px", md: "61px" }}
              background="transparent linear-gradient(237deg, #EBE5E5 0%, #E3DDE1 86%, #EAE4E4 100%) 0% 0% no-repeat padding-box"
              boxShadow="3px 3px 6px #00000029"
              borderRadius="20px"
              padding={{ base: "20px", md: "45px" }}
              marginBottom="60px"
            >
              <Flex
                direction="row"
                color="#242B45"
                alignItems="center"
                fontWeight="600"
                gap="5px"
                fontSize={{ base: "13px", md: "14px" }}>
                <ChevronLeftIcon fontSize={{ base: "20px", md: "24px" }} fontWeight="800" />
                <Text>BACK</Text>
              </Flex>
              <Flex direction="row-reverse" color="#242B45">
                <Text fontSize={{ base: "13px", md: "16px" }}>#16</Text>
              </Flex>
              <Flex direction={{ base: "column", lg: "row" }} gap={{ base: "0px", md: "75px" }}>
                <Flex width={{ base: "100%", lg: "50%" }} direction="column">
                  <Text
                    color="#242B45"
                    letterSpacing="var(--unnamed-character-spacing-0)"
                    fontSize={{ base: "18px", md: "24px" }}
                    fontWeight="700">Lorem ipsum dolor sit amet, consectetur adipiscing elit</Text>
                  <Flex
                    flex="1"
                    justifyContent="space-between"
                    color="#4B434B"
                    fontSize="14px"
                    marginTop="46px">
                    <Flex direction="column" justifyContent="space-evenly" fontWeight="600" fontSize={{ base: "11px", md: "16px" }} >
                      <Text
                        fontWeight="700"
                        fontSize={{ base: "13px", md: "18px" }}
                        letterSpacing="0px"
                        paddingBottom="26px"
                      >TOTAL RAISED</Text>
                      <Text paddingBottom="15px">Auction Currency</Text>
                      <Text paddingBottom="15px">Price per unit,</Text>
                      <Text paddingBottom="26px">Listing time</Text>
                    </Flex>
                    <Flex direction="column" justifyContent="space-evenly" fontSize={{ base: "11px", md: "16px" }}>
                      <Text fontSize={{ base: "13px", md: "18px" }} paddingBottom="26px">1000 ROSE</Text>
                      <Text paddingBottom="15px">ROSE</Text>
                      <Text paddingBottom="15px">0.01</Text>
                      <Text paddingBottom="26px">TBA</Text>
                    </Flex>
                  </Flex>
                  <Flex
                    width="100%"
                    height="2px"
                    background="transparent linear-gradient(90deg, #FF9AE8 0%, #BDFD9A 100%) 0% 0% no-repeat padding-box">
                  </Flex><Text
                    fontSize={{ base: "11px", md: "20px" }}
                    letterSpacing="0px"
                    fontWeight="700"
                    paddingTop="26px"
                    paddingBottom="26px"
                    color="#231F20"
                  >Token Information  </Text>
                  <Flex flex="1" justifyContent="space-between" color="#4B434B" fontSize={{ base: "10px", md: "16px" }}>
                    <Flex direction="column" justifyContent="space-evenly" fontWeight="600">

                      <Text
                        paddingBottom="15px">Contract Address</Text>
                      <Text
                        paddingBottom="15px">Token Symbol</Text>
                      <Text
                        paddingBottom="15px">Swap Ratio</Text>
                      <Text
                        paddingBottom="15px">Maximum Allocation per Wallet</Text>
                      <Text
                        paddingBottom="26px">Participants</Text>
                    </Flex>
                    <Flex direction="column" overflow="hidden" maxWidth="50%" textAlign="right" justifyContent="space-evenly">

                      <Text paddingBottom="15px" whiteSpace="nowrap">0xc15dfb24667470189fcaf3d93cb8b374b0c5e31a</Text>
                      <Text paddingBottom="15px">LIFE</Text>
                      <Text paddingBottom="15px">1 LIFE = 0.1$</Text>
                      <Text paddingBottom="15px">No Limits</Text>
                      <Text paddingBottom="26px">Public</Text>
                    </Flex>
                  </Flex>
                  <Flex
                    width="100%"
                    height="2px"
                    background="transparent linear-gradient(90deg, #FF9AE8 0%, #BDFD9A 100%) 0% 0% no-repeat padding-box">
                  </Flex>
                  <Flex flex="1" justifyContent="space-between" color="#4B434B" fontSize={{ base: "10px", md: "16px" }}
                    paddingTop="26px">
                    <Flex direction="column" justifyContent="space-evenly" fontWeight="600">

                      <Text marginBottom="15px">Running Time</Text>
                      <Text marginBottom="15px">Start</Text>
                      <Text marginBottom="15px">End</Text>
                    </Flex>
                    <Flex direction="column" justifyContent="space-evenly" textAlign="right">
                      <Text marginBottom="15px">28 Days 2 Hours 8 Minutes</Text>
                      <Text marginBottom="15px">2022. 04. 14., 11:00AM</Text>
                      <Text marginBottom="15px">2022. 05. 12., 11:00AM</Text>
                    </Flex>
                  </Flex>
                </Flex>
                <Flex direction="column" marginTop="45px" color="#4B434B" flex="1">
                  <Flex
                    fontWeight="700"
                    fontSize={{ base: "13px", md: "24px" }}
                    justifyContent="space-between">
                    <Text
                      color="#4B434B"
                      letterSpacing="0"
                      textDecoration="underline">Join The Pool</Text>
                    <Text display={{ base: "inline-block", md: "none" }} color="progressBlue.500">UPCOMING</Text>
                  </Flex>
                  <Flex marginTop="24px" fontSize={{ base: "10px", md: "16px" }} marginBottom="13px" fontWeight="600" justifyContent="space-between">
                    <Text>Status</Text>
                    <Text>CLOSED</Text>
                  </Flex>
                  <Progress width="100%" value="50" background="rgba(21, 90, 193, 0.3)" />

                  <Flex marginTop="24px" justifyContent="space-between" fontSize={{ base: "11px", md: "16px" }}>
                    <Text fontWeight="600">Current Participants</Text>
                    <Text textAlign="right">120</Text>
                  </Flex>

                  <Flex marginTop="24px" justifyContent="space-between" fontSize={{ base: "11px", md: "16px" }}>
                    <Text fontWeight="600">Your Bid Amount</Text>
                    <Text textAlign="right">Balance: 1000 ROSE</Text>
                  </Flex>

                  <InputGroup>
                    <Input
                      width="100%"
                      background="transparent linear-gradient(95deg, #23009639 0%, #5D53534D 100%) 0% 0% no-repeat padding-box"
                      border="1px solid #FFFFFF26"
                      borderRadius="10px"
                      height={{ base: "45px", md: "55px" }}
                      paddingRight="145px"
                      backdropFilter="blur(17px)" />
                    <InputRightElement width="auto" paddingRight="19px" paddingTop="12px" children={
                      <Flex alignItems="center"
                        gap="19px" fontSize={{ base: "10px", md: "14px" }}>
                        <Text
                          background="#FF7495 0% 0% no-repeat padding-box"
                          borderRadius="20px"
                          padding="5px 17px 6px 17px"
                        >Max.</Text>
                        <Text>ROSE</Text>
                      </Flex>
                    } />
                  </InputGroup>
                  <Flex direction="row"
                    padding={{ base: "50px 0px", md: "50px" }}
                  >
                    <Button
                      background="transparent linear-gradient(83deg, #FF7495 0%, #A0DF86 100%) 0% 0% no-repeat padding-box"
                      height={{ base: "45px", md: "57px" }}
                      width="100%"
                      color="#4B434B"
                      fontWeight="400"
                      borderRadius="11px"
                      disabled={true}
                    >Place a Bid</Button>
                  </Flex>
                </Flex>
              </Flex>
            </Flex>
          </Flex>
        </Flex>
      </Flex>

    </Box>
  </>

);

BidDetailPage.propTypes = {};

BidDetailPage.defaultProps = {};

export default BidDetailPage;
