import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import styles from './LaunchPad.module.scss';
import { Tabs, Tab, TabPanel, TabPanels, TabList, Box, Flex, Input, InputGroup, InputRightElement, Text, SimpleGrid } from '@chakra-ui/react';
import SideBar from '../../components/SideBar/SideBar';
import HeaderOptions from '../../components/HeaderOptions/HeaderOptions';
import { ArrowBackIcon, ArrowForwardIcon, SearchIcon } from '@chakra-ui/icons';
import LaunchPadCard from '../../components/LaunchPadCard/LaunchPadCard';
import MobileHeader from '../../components/MobileHeader/MobileHeader';
import CryticalBackground from '../../components/CryticalBackground/CryticalBackground';
import axios from 'axios';
const LaunchPad = () => {
  const [data, setData] = useState({
    'all': [],
    'upcoming': [],
    'closed': []
  });
  const componentDidMount = () => {
    axios
      .get(`${process.env.REACT_APP_ENDPOINT}/home/1`)
      .then((res) => {
        if (res.data.status === true) {
          const all = res.data.data;
          const upco = [];
          const clo = [];
          all.map((tx) => {
            if (tx.status === "UPCOMING") {
              upco.push(tx);
            }
            if (tx.status === "CLOSED") {
              clo.push(tx);
            }
          });
          setData({
            'all': all,
            'upcoming': upco,
            'closed': clo
          });
        }
      });
  }
  useEffect(componentDidMount, []);
  return (
    <>
      <CryticalBackground />
      <Box maxWidth="100vw" w="100vw" h="100vh" overflow="auto" >
        <Flex direction="column">
          <MobileHeader />
          <Flex color='white' height="100%">
            <SideBar active="5" />
            <Flex paddingLeft={{ base: "20px", md: "247px" }}
              paddingRight={{ base: "20px", md: "0px" }}
              flex="1" overflow="auto" flexDirection="column">
              <Flex flexDirection="column" >
                <HeaderOptions />
                <Flex
                  direction="row-reverse"
                  paddingRight={{ base: "0px", md: "48px" }}>
                  <InputGroup width={{ base: '100%', md: "477px" }}>
                    <Input
                      placeholder="Search"
                      background="transparent 0% 0% no-repeat padding-box"
                      border="1px solid #FFFFFF26"
                      borderRadius="20px"
                      opacity="1"
                      color="#434343"
                      width="477px"
                      height="48px"
                      backdropFilter="brightness(.96)" />
                    <InputRightElement paddingTop="5px" paddingRight="22px" children={<SearchIcon fontSize="20px" color="#959595" />} />
                  </InputGroup>
                </Flex>
              </Flex>

              <Flex
                direction="column"
                flex="1"
                width="100%"
                justifyContent="space-between"
                paddingBottom="50px"
              >
                <Tabs
                  display="flex"
                  maxWidth="100%"
                  flexDirection="column"
                  width="100%"
                  padding={{ base: "32px 0px", "2xl": "32px 58px 32px 58px" }} variant="unstyled">
                  <TabList border="none" color="#565555" colorScheme="#565555">
                    <Tab _selected={{ color: '#565555', fontWeight: "bold" }}>ALL</Tab>
                    <Tab _selected={{ color: '#565555', fontWeight: "bold" }}>UPCOMING</Tab>
                    <Tab _selected={{ color: '#565555', fontWeight: "bold" }}>CLOSED</Tab>
                  </TabList>
                  <TabPanels
                    maxWidth="100%"
                    width="100%"
                    flex="1">
                    <TabPanel width="100%" maxWidth="100%">
                      <SimpleGrid columns={{ base: 1, lg: 2, xl: 3 }} spacing={"20px"} >
                        {data.all.map((tx) =>
                          <LaunchPadCard type={
                            tx.status === "UPCOMING" ?
                              "upcoming" :
                              tx.status === "CLOSED" ?
                                "closed" : "live"
                          } />
                        )}
                      </SimpleGrid>
                    </TabPanel>

                    <TabPanel maxWidth="100%">
                      <SimpleGrid columns={{ base: 1, lg: 2, xl: 3 }} spacing={"20px"}>
                        {data.upcoming.map((tx) =>
                          <LaunchPadCard type="upcoming" />
                        )}
                      </SimpleGrid>
                    </TabPanel>

                    <TabPanel maxWidth="100%">
                      <SimpleGrid columns={{ base: 1, lg: 2, xl: 3 }} spacing={"20px"}>
                        {data.closed.map((tx) =>
                          <LaunchPadCard type="closed" />
                        )}
                      </SimpleGrid>
                    </TabPanel>

                  </TabPanels>

                </Tabs>
                <Flex
                  direction="row"
                  justifyContent="center"
                  color="#4B434B"
                  letterSpacing="0.22px"
                  fontSize={{ base: "15px", md: "18px" }}
                  fontWeight="600"
                  gap="60px">
                  <Flex direction="column" justifyContent="space-around">
                    <ArrowBackIcon fontSize="25px" />
                    <Text >Prev</Text>
                  </Flex>
                  <Flex direction="column" justifyContent="space-around">
                    <ArrowForwardIcon fontSize="25px" />
                    <Text>
                      Next
                    </Text>
                  </Flex>
                </Flex>
              </Flex>
            </Flex>
          </Flex>
        </Flex>

      </Box >
    </>

  );
}
LaunchPad.propTypes = {};

LaunchPad.defaultProps = {};

export default LaunchPad;
