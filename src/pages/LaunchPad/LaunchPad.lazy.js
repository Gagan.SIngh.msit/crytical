import React, { lazy, Suspense } from 'react';

const LazyLaunchPad = lazy(() => import('./LaunchPad'));

const LaunchPad = props => (
  <Suspense fallback={null}>
    <LazyLaunchPad {...props} />
  </Suspense>
);

export default LaunchPad;
