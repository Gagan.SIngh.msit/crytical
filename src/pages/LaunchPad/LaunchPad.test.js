import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import LaunchPad from './LaunchPad';

describe('<LaunchPad />', () => {
  test('it should mount', () => {
    render(<LaunchPad />);
    
    const launchPad = screen.getByTestId('LaunchPad');

    expect(launchPad).toBeInTheDocument();
  });
});