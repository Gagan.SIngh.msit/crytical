import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import Bonds from './Bonds';

describe('<Bonds />', () => {
  test('it should mount', () => {
    render(<Bonds />);
    
    const bonds = screen.getByTestId('Bonds');

    expect(bonds).toBeInTheDocument();
  });
});