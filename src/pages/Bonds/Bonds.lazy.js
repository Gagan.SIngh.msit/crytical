import React, { lazy, Suspense } from 'react';

const LazyBonds = lazy(() => import('./Bonds'));

const Bonds = props => (
  <Suspense fallback={null}>
    <LazyBonds {...props} />
  </Suspense>
);

export default Bonds;
