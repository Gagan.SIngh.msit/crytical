import React, { useEffect, useState } from 'react';
import { Box, Button, Flex, Text } from '@chakra-ui/react';
import SideBar from '../../components/SideBar/SideBar';
import HeaderOptions from '../../components/HeaderOptions/HeaderOptions';
import styles from './Bonds.module.scss';
import BondsInfoBox from '../../components/BondsInfoBox/BondsInfoBox';
import BondsInfoTextBox from '../../components/BondsInfoTextBox/BondsInfoTextBox';
import MobileHeader from '../../components/MobileHeader/MobileHeader';
import CryticalBackground from '../../components/CryticalBackground/CryticalBackground';
import axios from 'axios';
const Bonds = () => {
  const [state, setState] = useState(null);
  const componentDidMount = () => {
    axios
      .get(`${process.env.REACT_APP_ENDPOINT}/bonds`)
      .then((res) => {
        if (res.data.status === true) {
          setState(res.data);
        }
      });
  }
  useEffect(componentDidMount, []);
  return (
    <>
      <CryticalBackground />
      {state !== null ?
        <Box maxWidth="100vw" w="100vw" h="100vh" overflow="auto" >
          <Flex direction="column">
            <MobileHeader />
            <Flex color='white' height="100%">
              <SideBar active="7" />
              <Flex paddingLeft={{ base: "0px", md: "247px" }} overflow="auto" flex="1" flexDirection="column">
                <Flex flexDirection="column">
                  <HeaderOptions />
                  <Box width="100%" >
                    <Text
                      paddingBottom={{ md: "25px" }}
                      fontStyle="var(--unnamed-font-style-normal) normal var(--unnamed-font-weight-medium) var(--unnamed-font-size-30)/var(--unnamed-line-spacing-46) var(--unnamed-font-family-poppins)"
                      fontSize={{ base: "16px", md: "30px" }}
                      paddingLeft={{ base: "20px", md: "58px" }}
                      color="var(--unnamed-color-565555)"
                      fontWeight="500"
                      fontFamily="'Poppins', sans-serif">Bonds</Text>
                  </Box>
                  <Flex direction="column"
                    flex="1"
                    gap="30px"
                    paddingLeft={{ base: "20px", md: "58px" }}
                    paddingRight={{ base: "20px", md: "58px" }}
                    paddingBottom="100px">
                    <Flex
                      height={{ base: "auto", lg: "100px" }} className={styles.Bonds__Cards}
                      paddingLeft={{ base: "18px", lg: "50px" }}
                      paddingRight="50px"
                      paddingTop={{ base: "18px", lg: "0px" }}
                      paddingBottom={{ base: "18px", lg: "0px" }}
                      direction={{ base: "column", md: "row" }}>
                      <Flex width={{ base: "100%", md: "50%" }}
                        height="100%"
                        justifyContent="space-between"
                        alignItems={{ base: "start", md: "center" }}
                        fontSize="22px"
                        paddingRight="50px"
                        direction={{ base: "column", md: "row" }}>
                        <Text
                          color="#565555"
                          fontSize={{ base: "16px", md: "22px" }}
                        >Protocol Owned Liquidity:</Text>
                        <Text
                          color="#FF7495"
                          fontWeight="500"
                          fontSize="24px">{state.ProtocalOwnedLiquidity}</Text>
                      </Flex>
                      <Flex width={{ base: "100%", md: "50%" }}
                        height="100%"
                        justifyContent="space-between"
                        alignItems={{ base: "start", md: "center" }}
                        fontSize="22px"
                        direction={{ base: "column", md: "row" }}>
                        <Text
                          color="#565555"
                          fontSize={{ base: "16px", md: "22px" }}
                        >LP</Text>
                        <Text
                          color="#FF7495"
                          fontWeight="500"
                          fontSize="24px">{state.LP}</Text>
                      </Flex>
                    </Flex>
                    <Flex
                      direction={{ base: "column", lg: "row" }}
                      gap="58px"
                      justifyContent="space-between"
                    >
                      <Flex
                        flex="1"
                        direction={{ base: "column", lg: "row" }}
                        justifyContent="space-evenly"
                        className={styles.Bonds__Cards}
                        maxW={{ base: "100%", lg: "50%" }}>
                        <BondsInfoBox
                          title="You Give" />
                        <BondsInfoBox
                          title="You Get" />
                        <BondsInfoBox
                          title="Vesting Schedule"
                          subtitle={state.VestingSchedule} />
                      </Flex>
                      <Flex
                        justifyContent={{ base: "-moz-initial", md: "space-around" }}
                        className={styles.Bonds__Cards}
                        flex="1"
                        maxW={{ base: "100%", lg: "50%" }}>
                        <BondsInfoBox
                          title="Your Vesting Timer"
                          subtitle="Connect your wallet" />
                      </Flex>
                    </Flex>
                    <Flex
                      className={styles.Bonds__Cards}
                      paddingLeft={{ base: "20px", lg: "0px" }}
                      direction="column">
                      <Flex
                        flexDir={{ base: "column", lg: "row" }}
                        paddingTop="50px"
                        justifyContent="space-evenly">
                        <BondsInfoTextBox
                          title="Your LP Balance"
                          subtitle={state.LPBalance[0]}
                          caption={state.LPBalance[1]}
                        />
                        <BondsInfoTextBox
                          title="Payout Asset"
                          subtitle={state.PayoutAsset}
                        />
                        <BondsInfoTextBox
                          title="Current Market Price"
                          subtitle={state.CurrentMarketPrice} />
                      </Flex>
                      <Flex
                        paddingTop={{ base: "0px", md: "50px" }}
                        justifyContent="space-evenly"
                        flexDir={{ base: "column", lg: "row" }}
                      >
                        <BondsInfoTextBox
                          title="Bond Price"
                          subtitle={state.BondPrice}
                        />
                        <BondsInfoTextBox
                          title="Discount"
                          subtitle={state.Discount}
                          icon={true} />
                      </Flex>
                      <Flex
                        paddingTop="50px"
                        justifyContent="space-evenly"
                        paddingBottom="18px">
                        <Button
                          className={styles.Bonds__Button}>Bond</Button>
                      </Flex>
                    </Flex>
                  </Flex>
                </Flex>
              </Flex>
            </Flex>
          </Flex>

        </Box >
        : null
      }

    </>

  );
}
Bonds.propTypes = {};

Bonds.defaultProps = {};

export default Bonds;
