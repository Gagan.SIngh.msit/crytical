import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import {
  Box,
  Button,
  Container,
  Flex,
  Link,
  Select,
  Text,
  TableContainer,
  Table,
  Thead,
  Tr,
  Th,
  Tbody,
  Tfoot,
  Td,
  Tabs,
  TabList,
  Tab,
  TabPanels,
  TabPanel,
  Image
} from '@chakra-ui/react';
import styles from './Home.module.scss';
import SideBar from '../../components/SideBar/SideBar';

import BlackNFTImage from "../../assets/images/CRYTICAL_NFT_Black.png";
import HomeLabel from '../../components/HomeLabel/HomeLabel';
import CryticalLineChart from '../../components/CryticalLineChart/CryticalLineChart';
import CryticalAreaChart from '../../components/CryticalAreaChart/CryticalAreaChart';
import HeaderOptions from '../../components/HeaderOptions/HeaderOptions';
import CryticalBackground from '../../components/CryticalBackground/CryticalBackground';
import MobileHeader from '../../components/MobileHeader/MobileHeader';
import axios from 'axios';
import CandleStickGraph from '../../components/CandleStickGraph/CandleStickGraph';
const Home = () => {
  const tableStyle = { base: "7px", md: "var(--chakra-space-4)" };
  const [data, setData] = useState(null);
  const [tierData, setTierData] = useState(null);
  const componentDidMount = () => {
    axios
      .get(`${process.env.REACT_APP_ENDPOINT}/home`)
      .then((res) => {
        if (res.data.success === true) {
          setData(res.data.data);
        }
      });
  }
  const getTiers = () => {
    if (data === null || data === undefined) return;
    let tiers = data.tiers;
    let tData = [];
    for (let i = 0; i < tiers.length; ++i) {
      let tier = tiers[i];
      let t = [];
      for (let prop in tier) {
        t.push(tier[prop]);
      }
      tData.push(t);
    }
    setTierData(tData);
  }
  useEffect(getTiers, [data]);
  useEffect(componentDidMount, []);
  return (
    <>

      <CryticalBackground />
      {data !== null ?
        <Box maxWidth="100vw" w="100vw" h="100vh" overflow="auto" >
          <Flex direction="column">
            <MobileHeader />
            <Flex color='white' height="100%">
              <SideBar active="1" />
              <Flex paddingLeft={{ base: "0", md: "247px" }} overflow="auto" flex="1" flexDirection="column">
                <Flex flexDirection="column">
                  <HeaderOptions />
                  <Box width="100%">
                    <Text
                      paddingBottom={{ md: "25px" }}
                      fontStyle="var(--unnamed-font-style-normal) normal var(--unnamed-font-weight-medium) var(--unnamed-font-size-30)/var(--unnamed-line-spacing-46) var(--unnamed-font-family-poppins)"
                      fontSize={{ base: "16px", md: "30px" }}
                      paddingLeft={{ base: "20px", lg: "58px" }}
                      color="var(--unnamed-color-565555)"
                      fontWeight="500">Home</Text>
                  </Box>
                </Flex>
                <Flex
                  flexDirection={{ base: "column", lg: "row" }}
                  paddingLeft={{ base: "20px", lg: "58px" }}
                  paddingRight={{ base: "20px", lg: "58px" }}
                  direction="row"
                  flex="1"
                  gap="6">
                  <Flex
                    direction="column"
                    background="transparent 0% 0% no-repeat padding-box"
                    boxShadow="inset 0px 3px 6px #00000029"
                    border="1px solid #FFFFFF26"
                    borderRadius="20px"
                    backdropFilter="brightness(.96)"
                    width={{ base: "100%", lg: "33%" }}
                    maxWidth="100%"
                    maxHeight={{ base: "850px", lg: "auto" }}
                    overflow="hidden"
                  >
                    <Box
                      width="100%"
                      height="auto"
                      marginTop="27px"
                      paddingBottom={{ base: "22px", md: "38px" }}>
                      <Image marginLeft="auto"
                        height={{ base: "245px", lg: "415px" }}
                        marginRight="auto"
                        marginTop={{ small: "26px", md: "44px" }} fit="contain" src={BlackNFTImage} />
                    </Box>
                    <Flex
                      flexDir="row"
                      justifyContent="center"
                      fontSize={{ base: "9px", md: "14px" }}
                      color="#565555"
                      fontWeight="700"
                      width="100%"
                      gap="50px"
                      paddingBottom="26px">
                      <Text>Referrals Below:</Text>
                      <Text>12</Text>
                    </Flex>
                    <TableContainer width={{ base: "100%" }}  >
                      <Table variant="unstyled" size='sm' >
                        <Thead color="#565555">
                          <Tr fontWeight="600">
                            <Th
                              className={styles.Home__table}
                              paddingLeft={tableStyle}
                              paddingRight={tableStyle}
                              fontSize={{ base: "8px", md: "0.75rem" }} >Tiers</Th>
                            <Th
                              className={styles.Home__table}
                              paddingLeft={tableStyle}
                              paddingRight={tableStyle}
                              fontSize={{ base: "8px", md: "0.75rem" }}>Numbers</Th>
                            <Th
                              className={styles.Home__table}
                              paddingLeft={tableStyle}
                              paddingRight={tableStyle}
                              fontSize={{ base: "8px", md: "0.75rem" }}>Staked Amount</Th>
                            <Th
                              paddingLeft={tableStyle}
                              paddingRight={tableStyle}
                              fontSize={{ base: "8px", md: "0.75rem" }}>Value Staked</Th>
                          </Tr>
                        </Thead>
                        <Tbody color="#565555">
                          {tierData !== null ?
                            tierData.map((tx) =>
                              <Tr>{
                                tx.map((t, i) =>
                                  (i === tx.length - 1) ?
                                    <Td paddingLeft={tableStyle}
                                      paddingRight={tableStyle}
                                      fontSize={{ base: "8px", md: "0.75rem" }}>{t}</Td>
                                    :
                                    <Td className={styles.Home__table} paddingLeft={tableStyle}
                                      paddingRight={tableStyle}
                                      fontSize={{ base: "8px", md: "0.75rem" }}>{t}</Td>)}</Tr>)
                            : null}

                        </Tbody>
                      </Table>
                    </TableContainer>
                  </Flex>
                  <Flex
                    direction="column"
                    width={{ base: "100%", lg: "66%" }}
                  >
                    <Flex
                      width="100%"
                      background="transparent 0% 0% no-repeat padding-box"
                      boxShadow="inset 0px 3px 6px #00000029"
                      border="1px solid #FFFFFF26"
                      borderRadius={{ base: "15px", md: "20px" }}
                      backdropFilter="brightness(.96)"
                      minHeight={{ base: "auto", md: "454px" }}>
                      <Tabs
                        display="flex"
                        maxWidth="100%"
                        flexDirection="column"
                        width="100%"
                        padding={{ sm: "0px", md: "32px 20px 32px 20px" }} variant="unstyled">
                        <TabList border="none" color="#565555" colorScheme="#565555">
                          <Tab fontSize={{ base: "10px", md: "1rem" }} _selected={{ color: '#565555', fontWeight: "bold" }}>Token Price</Tab>
                          <Tab fontSize={{ base: "10px", md: "1rem" }} _selected={{ color: '#565555', fontWeight: "bold" }}>Market Cap</Tab>
                          <Tab fontSize={{ base: "10px", md: "1rem" }} _selected={{ color: '#565555', fontWeight: "bold" }}>Total Supply</Tab>
                        </TabList>
                        <TabPanels maxWidth="100%" width="100%" flex="1" >
                          <TabPanel maxWidth="100%">
                            <CandleStickGraph labels={data.TokenPrice.labels}
                              values={data.TokenPrice.data}
                              values2={data.TokenPrice.data} />
                          </TabPanel>
                          <TabPanel>
                            <CryticalAreaChart labels={data.MarketCap.labels} values={data.MarketCap.data} />
                          </TabPanel>
                          <TabPanel><CryticalAreaChart labels={data.totalSupply.labels} values={data.totalSupply.data} isFill={false} /></TabPanel>
                        </TabPanels>
                      </Tabs>
                    </Flex>
                    <Flex
                      flexDirection="column"
                      width="100%">
                      <Flex direction={{ base: "column", lg: "row" }}
                        width="100%" gap={{ base: 1, lg: 3 }}
                        paddingTop={{ base: "0px", md: "105px" }}>

                        <HomeLabel title="Total Supply" left_title={data.TotalSupply} right_title="" />
                        <HomeLabel title="Circulating Supply" left_title={data["TUL/TUD"]} right_title="" />
                        <HomeLabel title="TUL/TUD" left_title="163,276,974" right_title="" />
                      </Flex>
                      <Flex direction={{ base: "column", lg: "row" }}
                        marginBottom={{ base: "50px", lg: "0px" }}
                        width="100%" gap={{ base: 1, lg: 3 }} paddingTop={{ base: "0px", md: "105px" }}>

                        <HomeLabel title="Wallet Balance" left_title={data.WalletBalance[0]} right_title={data.WalletBalance[1]} />
                        <HomeLabel title="Staked Balance" left_title={data["Staked Balance"][0]} right_title={data["Staked Balance"][1]} />
                        <HomeLabel title="Earnings" left_title={data.Earnings[0]} right_title={data.Earnings[1]} />
                      </Flex>
                    </Flex>
                  </Flex>
                </Flex>
              </Flex>
            </Flex>
          </Flex>
        </Box > : null}
    </>

  );
}

Home.propTypes = {};

Home.defaultProps = {};

export default Home;
