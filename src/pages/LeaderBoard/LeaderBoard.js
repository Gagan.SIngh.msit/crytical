import React, { useEffect, useState } from 'react';
import { Flex, Box, Text, Image } from '@chakra-ui/react';
import SideBar from '../../components/SideBar/SideBar';
import HeaderOptions from '../../components/HeaderOptions/HeaderOptions';
import CryticalTable from '../../components/CryticalTable/CryticalTable';
import PropTypes from 'prop-types';
import CryticalBlack from "../../assets/images/CRYTICAL_NFT_Black.png";
import CryticalDiamond from "../../assets/images/CRYTICAL_NFT_Diamond.png";
import CryticalGold from "../../assets/images/CRYTICAL_NFT_Gold.png";
import CryticalSilver from "../../assets/images/CRYTICAL_NFT_Silver.png";
import styles from './LeaderBoard.module.scss';
import MobileHeader from '../../components/MobileHeader/MobileHeader';
import CryticalBackground from '../../components/CryticalBackground/CryticalBackground';
import axios from 'axios';
import BronzeNFTImage from "../../assets/images/BRONZE_Crop.png";

const LeaderBoard = () => {
  const [state, setState] = useState(null);
  const [tData, setTData] = useState(null);
  const componentDidMount = () => {
    axios
      .get(`${process.env.REACT_APP_ENDPOINT}/leaderBoard`)
      .then((res) => {
        if (res.data.success === true) {
          setState(res.data.data);
        }
      });
  }
  const setTableData = () => {
    if (state === null || state === undefined) return;
    let black = state.ranking.black;
    let fb = [];
    for (let i = 0; i < black.USERNAME.length; ++i) {
      fb.push([
        black["#"][i],
        black["USERNAME"][i],
        black["NEWREFERRALS(THIS WEEK)"][i],
        black["STAKEDAMOUNT"][i],
      ]);
    }
    let diamond = state.ranking.diamond;
    let fd = [];
    for (let i = 0; i < diamond.USERNAME.length; ++i) {
      fd.push([
        diamond["#"][i],
        diamond["USERNAME"][i],
        diamond["NEWREFERRALS(THIS WEEK)"][i],
        diamond["STAKEDAMOUNT"][i],
      ]);
    }
    let gold = state.ranking.gold;
    let fg = [];
    for (let i = 0; i < gold.USERNAME.length; ++i) {
      fg.push([
        gold["#"][i],
        gold["USERNAME"][i],
        gold["NEWREFERRALS(THIS WEEK)"][i],
        gold["STAKEDAMOUNT"][i],
      ]);
    }
    let silver = state.ranking.silver;
    let fs = [];
    for (let i = 0; i < silver.USERNAME.length; ++i) {
      fs.push([
        silver["#"][i],
        silver["USERNAME"][i],
        silver["NEWREFERRALS(THIS WEEK)"][i],
        silver["STAKEDAMOUNT"][i],
      ]);
    }
    let bronze = state.ranking.bronze;
    let fbro = [];

    for (let i = 0; i < bronze.USERNAME.length; ++i) {
      fbro.push([
        bronze["#"][i],
        bronze["USERNAME"][i],
        bronze["NEWREFERRALS(THIS WEEK)"][i],
        bronze["STAKEDAMOUNT"][i],
      ]);
    }
    setTData({
      black: fb,
      diamond: fd,
      gold: fg,
      silver: fs,
      bronze: fbro
    })
  }
  useEffect(setTableData, [state]);
  useEffect(componentDidMount, []);
  return (
    <>
      <CryticalBackground />
      {state !== null ?
        <Box maxWidth="100vw" w="100vw" h="100vh" overflow="auto" >
          <Flex direction="column">
            <MobileHeader />
            <Flex color='white' height="100%">
              <SideBar active="4" />
              <Flex paddingLeft={{ base: "0px", md: "247px" }} overflow="auto" flex="1" flexDirection="column">
                <Flex flexDirection="column" q>
                  <HeaderOptions />

                  <Box width="100%" height="43px">
                    <Text
                      fontSize={{ base: "16px", md: "30px" }}
                      paddingLeft={{ base: "20px", md: "58px" }}
                      color="var(--unnamed-color-565555)"
                      fontWeight="500"
                      fontFamily="'Poppins', sans-serif">Leaderboard</Text>
                  </Box>
                  <Flex flex="1"
                    paddingLeft={{ base: "20px", md: "58px" }}
                    paddingRight={{ base: "20px", md: "58px" }}
                    flexDirection={{ base: "column", xl: "row" }}
                    gap={{ base: "0px", lg: "50px" }}
                    justifyContent={{ lg: "space-evenly" }}>
                    <Flex direction="column">
                      <Text
                        className={styles.LeaderBoard__Text}
                        fontSize={{ base: "16px", md: "18px" }}
                        marginTop={{ base: "05px", md: "50px" }}>
                        Weekly Leaderboard per Rank
                      </Text>
                      <Image marginTop="9px" display={{ base: "block", md: "none" }} src={CryticalBlack} height="146px" fit="contain" />
                      <CryticalTable
                        stretch={true}
                        labels={['#', 'USERNAME', 'NEW REFERRALS (THIS WEEK)', 'STAKED AMOUNT']}
                        data={
                          tData !== null ? tData.black : []
                        }
                      />
                      <Image marginTop="9px" display={{ base: "block", md: "none" }} src={CryticalDiamond} height="146px" fit="contain" />
                      <CryticalTable
                        type="Diamond"
                        stretch={true}
                        labels={['#', 'USERNAME', 'NEW REFERRALS (THIS WEEK)', 'STAKED AMOUNT']}
                        data={
                          tData !== null ? tData.diamond : []
                        }
                      />
                      <Image marginTop="9px" display={{ base: "block", md: "none" }} src={CryticalGold} height="146px" fit="contain" />
                      <CryticalTable
                        type="Gold"
                        stretch={true}
                        labels={['#', 'USERNAME', 'NEW REFERRALS (THIS WEEK)', 'STAKED AMOUNT']}
                        data={
                          tData !== null ? tData.gold : []
                        }
                      />
                      <Image marginTop="9px" display={{ base: "block", md: "none" }} src={CryticalSilver} height="146px" fit="contain" />
                      <CryticalTable
                        type="Silver"
                        stretch={true}
                        labels={['#', 'USERNAME', 'NEW REFERRALS (THIS WEEK)', 'STAKED AMOUNT']}
                        data={
                          tData !== null ? tData.silver : []
                        }
                      />
                      <Image marginTop="9px" display={{ base: "block", md: "none" }} src={BronzeNFTImage} height="146px" fit="contain" />
                      <CryticalTable
                        type="Bronze"
                        stretch={true}
                        labels={['#', 'USERNAME', 'NEW REFERRALS (THIS WEEK)', 'STAKED AMOUNT']}
                        data={
                          tData !== null ? tData.silver : []
                        }
                      />
                    </Flex>
                    <Flex marginTop={{ base: "40px", md: "100px" }} flexDirection="column">
                      <Text className={styles.LeaderBoard__Text} fontSize={{ base: "16px", md: "18px" }}>Weekly Prize Amount</Text>
                      <Flex className={styles.LeaderBoard__Card}
                        direction="column"
                        w={{ base: "100%", md: "100%" }}
                        paddingLeft={{ base: "39px", md: "61px" }}>
                        <Text className={styles.LeaderBoard__Card__MainText} fontSize={{ base: "51px", md: "80px" }}>
                          {state.weeklyprize[0]}
                        </Text>
                        <Text className={styles.LeaderBoard__Card__SubText} fontSize={{ base: "9px", md: "15px" }}>

                          {state.weeklyprize[1]}
                        </Text>
                      </Flex>
                      <Text className={styles.LeaderBoard__Text}>Countdown for LB Prizes</Text>
                      <Flex className={styles.LeaderBoard__Card}
                        paddingLeft={{ base: "39px", md: "61px" }}
                        paddingRight={{ base: "39px", md: "61px" }}
                        w={{ base: "100%", md: "100%" }} direction="column">

                        <Text
                          className={styles.LeaderBoard__Card__SubText}
                          fontSize={{ base: "9px", md: "15px" }}
                        >Upcoming prizes in</Text>
                        <Flex direction="row">
                          <Flex direction="column" alignItems="center">
                            <Text className={styles.LeaderBoard__Card__MainText} fontSize={{ base: "51px", md: "80px" }}>
                              {state.countdown[0]}
                            </Text>
                            <Text
                              className={styles.LeaderBoard__Card__SubText}
                              fontSize={{ base: "9px", md: "15px" }}
                            >
                              hours
                            </Text>
                          </Flex>
                          <Flex direction="column" alignItems="center">
                            <Text className={styles.LeaderBoard__Card__MainText} fontSize={{ base: "51px", md: "80px" }}>
                              :{state.countdown[1]}
                            </Text>
                            <Text
                              className={styles.LeaderBoard__Card__SubText}
                              fontSize={{ base: "9px", md: "15px" }}>
                              mins
                            </Text>
                          </Flex>
                          <Flex direction="column" alignItems="center">
                            <Text className={styles.LeaderBoard__Card__MainText} fontSize={{ base: "51px", md: "80px" }}>
                              :{state.countdown[2]}
                            </Text>
                            <Text
                              className={styles.LeaderBoard__Card__SubText}
                              fontSize={{ base: "9px", md: "15px" }}>
                              secs
                            </Text>
                          </Flex>
                        </Flex>
                      </Flex>
                    </Flex>
                  </Flex>
                </Flex>
              </Flex>
            </Flex>
          </Flex>

        </Box>
        : null}

    </>
  );
}
LeaderBoard.propTypes = {};

LeaderBoard.defaultProps = {};

export default LeaderBoard;
