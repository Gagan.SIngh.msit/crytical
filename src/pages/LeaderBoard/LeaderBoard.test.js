import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import LeaderBoard from './LeaderBoard';

describe('<LeaderBoard />', () => {
  test('it should mount', () => {
    render(<LeaderBoard />);
    
    const leaderBoard = screen.getByTestId('LeaderBoard');

    expect(leaderBoard).toBeInTheDocument();
  });
});