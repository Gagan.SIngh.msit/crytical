import React, { lazy, Suspense } from 'react';

const LazyLeaderBoard = lazy(() => import('./LeaderBoard'));

const LeaderBoard = props => (
  <Suspense fallback={null}>
    <LazyLeaderBoard {...props} />
  </Suspense>
);

export default LeaderBoard;
