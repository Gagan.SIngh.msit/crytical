import React, { lazy, Suspense } from 'react';

const LazyPro = lazy(() => import('./Pro'));

const Pro = props => (
  <Suspense fallback={null}>
    <LazyPro {...props} />
  </Suspense>
);

export default Pro;
