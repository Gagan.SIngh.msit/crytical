import React, { useState, useEffect } from 'react';
import SideBar from '../../components/SideBar/SideBar';
import { Box, Flex, Text, Tab, Tabs, TabList, TabPanel, TabPanels, Divider } from '@chakra-ui/react';
import HeaderOptions from '../../components/HeaderOptions/HeaderOptions';
import CryticalLineChart from '../../components/CryticalLineChart/CryticalLineChart';
import CryticalAreaChart from '../../components/CryticalAreaChart/CryticalAreaChart';
import ProTextContainer from '../../components/ProTextContainer/ProTextContainer';
import CryticalTable from '../../components/CryticalTable/CryticalTable';
import { TriangleUpIcon } from '@chakra-ui/icons';
import MobileHeader from '../../components/MobileHeader/MobileHeader';
import CryticalProTable from '../../components/CryticalProTable/CryticalProTable';
import CryticalBackground from '../../components/CryticalBackground/CryticalBackground';
import axios from 'axios';
import CandleStickGraph from '../../components/CandleStickGraph/CandleStickGraph';

const Pro = () => {
  const labels = ['Timestamp', 'Price'];
  const data = [["01:00", "$12345"], ["02:00", "$12345"], ["03:00", "$12345"], ["04:00", "$12345"], ["05:00", "$12345"], ["06:00", "$12345"], ["07:00", "$12345"], ["08:00", "$12345"], ["09:00", "$12345"], ["10:00", "$12345"], ["11:00", "$12345"], ["12:00", "$12345"]];
  const [state, setState] = useState(null);
  const [tData, setTData] = useState(null);
  const componentDidMount = () => {
    axios
      .get(`${process.env.REACT_APP_ENDPOINT}/pro`)
      .then((res) => {
        if (res.data.success === true) {
          setState(res.data.data);
        }
      });
  }

  const setTableData = () => {
    if (state === null || state === undefined) return;
    const am = state.lists.AASHourlyLog.AM;
    const pm = state.lists.AASHourlyLog.PM;
    let fam = [];
    for (let i = 0; i < am.timestamp.length; ++i) {
      fam.push([am.timestamp[i], am.values[i]]);
    }
    let fpm = [];
    for (let i = 0; i < pm.timestamp.length; ++i) {
      fpm.push([pm.timestamp[i], pm.values[i]]);
    }
    setTData({
      'AM': fam,
      'PM': fpm
    });
  }
  useEffect(setTableData, [state]);
  useEffect(componentDidMount, []);
  return (
    <>
      <CryticalBackground />
      {state !== null ?
        <Box fontFamily="'Poppins', sans - serif" maxWidth="100vw" w="100vw" h="100vh" overflow="auto" >
          <Flex direction="column">
            <MobileHeader />
            <Flex color='white' height="100%">
              <SideBar active="2" />
              <Flex paddingLeft={{ base: "0px", md: "247px" }} overflow="auto" flex="1" flexDirection="column">
                <Flex flexDirection="column">
                  <HeaderOptions />
                  <Box width="100%">
                    <Text
                      fontStyle="var(--unnamed-font-style-normal) normal var(--unnamed-font-weight-medium) var(--unnamed-font-size-30)/var(--unnamed-line-spacing-46) var(--unnamed-font-family-poppins)"
                      fontSize={{ base: "16px", md: "30px" }}
                      paddingLeft={{ base: "20px", md: "58px" }}
                      color="var(--unnamed-color-565555)"
                      fontWeight="500">Pro</Text>
                  </Box>
                  <Flex flex="1" width="100%">
                    <Tabs
                      display="flex"
                      maxWidth="100%"
                      flexDirection="column"
                      width="100%"
                      padding={{ base: "0px", xl: "32px 58px 32px 58px" }} variant="unstyled">
                      <TabList marginLeft="100px" border="none" color="#565555" colorScheme="#565555">
                        <Tab _selected={{ color: '#565555', fontWeight: "bold" }}>CHARTS</Tab>
                        <Tab _selected={{ color: '#565555', fontWeight: "bold" }}>LISTS</Tab>
                      </TabList>
                      <TabPanels maxWidth="100%" width="100%" flex="1" >
                        <TabPanel width="100%">
                          <Box
                            width="100%"
                            background="transparent 0% 0% no-repeat padding-box"
                            boxShadow="inset 0px 3px 6px #00000029"
                            border="1px solid #FFFFFF26"
                            borderRadius="20px"
                            opacity="1"
                            backdropFilter="brightness(.96)"
                            padding={{ base: "18px", md: "27px 36px" }}>

                            <CryticalAreaChart
                              title="Daily AAS"
                              isFill="false"
                              labels={state.charts.DailyAAS.labels}
                              values={state.charts.DailyAAS.data}
                              subtitle={
                                <>
                                  <Text textAlign="justify">AAS: “Automatically Adjusted Supply” is Quota’s algorithmic equilibrium seeking token supply moderator. The price action of 4.0 is systematically logged in 1 hour intervals over a 24 hour period to determine the degree of deviation from the S&P 500. This deviation data is then processed in our Automatically Adjusted Supply (“AAS”) algorithm, to adjust the token supply accordingly.</Text>
                                  <Text textAlign="justify" paddingTop="22px" fontWeight="bold">*Automatic Supply Adjustment AAS occurs once a day at 12am UTC.</Text>
                                </>
                              }
                            />
                          </Box>
                          <Flex
                            direction={{
                              base: "column", xl: "row"
                            }}
                            gap="50px"
                            paddingTop="50px">
                            <Box
                              width={{ base: "100%", xl: "50%" }}
                              background="transparent 0% 0% no-repeat padding-box"
                              boxShadow="inset 0px 3px 6px #00000029"
                              border="1px solid #FFFFFF26"
                              borderRadius="20px"
                              opacity="1"
                              backdropFilter="brightness(.96)"
                              padding="27px 36px">
                              <CandleStickGraph
                                labels={state.charts.TokenPrice.labels}
                                values={state.charts.TokenPrice.data}
                              />
                            </Box>
                            <Box
                              width={{ base: "100%", xl: "50%" }}
                              background="transparent 0% 0% no-repeat padding-box"
                              boxShadow="inset 0px 3px 6px #00000029"
                              border="1px solid #FFFFFF26"
                              borderRadius="20px"
                              opacity="1"
                              backdropFilter="brightness(.96)"
                              padding="27px 36px">
                              <CryticalAreaChart
                                title="Market Cap"

                                labels={state.charts.MarketPrice.labels}
                                values={state.charts.MarketPrice.data}
                              />
                            </Box>
                          </Flex>
                        </TabPanel>
                        <TabPanel width="100%">
                          <Flex width="100%"
                            flexDirection={{ base: "column-reverse", xl: "row" }}>
                            <Flex width={{ base: "100%", xl: "50%" }} marginTop={{ base: "25px", md: "0px" }} direction="column">
                              <Flex justifyContent="space-between" width="100%" direction="row">
                                <Text
                                  fontStyle="normal normal normal 30px/46px Poppins"
                                  letterSpacing="0px"
                                  color="#565555"
                                  opacity="1"
                                  fontSize={{ base: "14px", md: "24px" }}
                                  fontWeight="500">Yesterday's AAS</Text>
                                <Text
                                  fontStyle="normal normal bold 25px/38px Poppins"
                                  letterSpacing="0px"
                                  color="#FF7495"
                                  opacity="1"
                                  fontWeight="600"
                                  fontSize={{ base: "14px", md: "25px" }}>
                                  <TriangleUpIcon /> 7%</Text>
                              </Flex>
                              <Flex
                                width="100%"
                                background="transparent 0% 0% no-repeat padding-box"
                                boxShadow="inset 0px 3px 6px #00000029"
                                border="1px solid #FFFFFF26"
                                borderRadius="20px"
                                opacity="1"
                                backdropFilter="brightness(.96)"
                                padding="27px 36px"
                                flexDirection="column">
                                <Text
                                  fontStyle="normal normal normal 30px/46px Poppins"
                                  letterSpacing="0px"
                                  color="#565555"
                                  opacity="1"
                                  fontSize="24px"
                                  fontWeight="600">AAS Hourly Log</Text>
                                <Flex
                                  flexDirection="row"
                                  width="100%"
                                >
                                  <CryticalProTable
                                    labels={labels}
                                    data={tData !== null ? tData.AM : []} />
                                  <Divider width="20px" background="#BABABA" orientation='vertical' border="border: 0.5px solid #BABABA" />
                                  <CryticalProTable
                                    labels={labels}
                                    data={tData !== null ? tData.PM : []} />
                                </Flex>

                              </Flex>
                            </Flex>
                            <Flex
                              width={{ base: "100%", xl: "50%" }}
                              flexWrap="wrap"
                              paddingTop={{ base: "0px", xl: "150px" }}
                              marginLeft={{ xl: "16px" }}
                              gap="6"
                              flexDirection="column"
                            >
                              <ProTextContainer
                                title="Current TWAP"
                                subtitle={state.lists.CurrentTWAP}
                                width="100%" />

                              <Flex width="100%" flexWrap="wrap" justifyContent="space-between" >
                                <ProTextContainer
                                  title="TVD/TVL"
                                  subtitle={state.lists["TVD/TVL"]}
                                  width="48%" />
                                <ProTextContainer
                                  title="Treasury Resource"
                                  subtitle={state.lists["Treasury Resource"]}
                                  width="47.5%" />
                              </Flex>
                              <Flex width="100%" flexWrap="wrap" justifyContent="space-between">
                                <ProTextContainer
                                  title="Liquidity Backed per token"
                                  subtitle={state.lists["LiquidityBackedpertoken"]}
                                  width="48%" />
                                <ProTextContainer
                                  title="Protocol and Liquidity"
                                  subtitle={state.lists["ProtocolandLiquidity"]}
                                  width="47.5%" />
                              </Flex>
                            </Flex>
                          </Flex>
                        </TabPanel>
                      </TabPanels>
                    </Tabs>
                  </Flex>
                </Flex>
              </Flex>
            </Flex>
          </Flex>

        </Box >
        : null}
    </>

  );
}

Pro.propTypes = {};

Pro.defaultProps = {};

export default Pro;
