import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import Pro from './Pro';

describe('<Pro />', () => {
  test('it should mount', () => {
    render(<Pro />);
    
    const pro = screen.getByTestId('Pro');

    expect(pro).toBeInTheDocument();
  });
});