import React, { useEffect, useState } from 'react';
import { Box, Flex, Text, Tabs, Tab, TabList, TabPanel, TabPanels, Input, Button, Image } from '@chakra-ui/react';
import CryticalBlack from "../../assets/images/CRYTICAL_NFT_Black.png";
import SideBar from '../../components/SideBar/SideBar';
import HeaderOptions from '../../components/HeaderOptions/HeaderOptions';
import NftInfoBox from '../../components/NFTInfoBox/NFTInfoBox';
import CryticalTable from '../../components/CryticalTable/CryticalTable';
import CryticalGrid from '../../components/CryticalGrid/CryticalGrid';
import MobileHeader from '../../components/MobileHeader/MobileHeader';
import CryticalBackground from '../../components/CryticalBackground/CryticalBackground';
import axios from 'axios';
const Nft = () => {
  const [state, setState] = useState(null);
  const [tData, setTData] = useState(null);
  const componentDidMount = () => {
    axios
      .get(`${process.env.REACT_APP_ENDPOINT}/pro/1`)
      .then((res) => {
        if (res.data.status === true) {
          setState(res.data);
        }
      });
  }

  const setTableData = () => {
    if (state === null || state === undefined) return;

    let black = state.Nft;
    let fb = [];
    for (let i = 0; i < black.USERNAME.length; ++i) {
      fb.push([
        black["#"][i],
        black["USERNAME"][i],
        black["NEWREFERRALS(THIS WEEK)"][i],
        black["STAKEDAMOUNT"][i],
      ]);
    }
    setTData(fb);
  }
  useEffect(componentDidMount, []);
  useEffect(setTableData, [state]);
  return (
    <>
      <CryticalBackground />
      {
        state !== null ?
          <Box maxWidth="100vw" w="100vw" h="100vh" overflow="auto" >
            <Flex direction="column">
              <MobileHeader />
              <Flex color='white' height="100%">
                <SideBar active="6" />
                <Flex
                  paddingLeft={{ base: "0px", md: "247px", }}
                  overflow="auto"
                  flex="1"
                  flexDirection="column">
                  <Flex flexDirection="column">
                    <HeaderOptions />
                    <Box width="100%" height="43px">
                      <Text
                        fontSize={{ base: "16px", md: "30px" }}
                        paddingLeft={{ base: "20px", md: "58px" }}
                        color="var(--unnamed-color-565555)"
                        fontWeight="500"
                        fontFamily="'Poppins', sans-serif">NFT</Text>
                    </Box>
                    <Flex flex="1" width="100%">
                      <Tabs
                        display="flex"
                        maxWidth="100%"
                        flexDirection="column"
                        width="100%"
                        padding={{ base: "0px", md: "32px 20px 32px 20px" }}
                        fontFamily="'Poppins', sans-serif" variant="unstyled">
                        <TabList marginLeft={{ base: "20px", md: "100px" }} border="none" color="#565555"
                          fontWeight="500" colorScheme="#565555" fontSize={{ base: "14px", md: "16px" }}>
                          <Tab color="#bababa" _selected={{ color: '#565555' }}>MEMBERSHIP</Tab>
                          <Tab color="#bababa" _selected={{ color: '#565555' }}>GALLERY</Tab>
                        </TabList>
                        <TabPanels maxWidth="100%" width="100%" flex="1" >
                          <TabPanel width="100%">
                            <Flex direction="column">
                              <Flex
                                width="100%"
                                direction={{ base: "column-reverse", xl: "row" }}>
                                <Flex flex="1" direction="column">
                                  <Flex
                                    flex="1"
                                    background="transparent 0% 0% no-repeat padding-box"
                                    border="1px solid #FFFFFF26"
                                    borderRadius="20px"
                                    backdropFilter="brightness(.96)"
                                    boxShadow="inset 0px 3px 6px #00000029"
                                    padding={{ base: "20px", lg: "36.55px 54.54px" }}
                                    marginRight={{ base: "0px", lg: "30px" }}
                                    flexDirection={{ base: "column", lg: "row" }}>
                                    <Flex
                                      flex="1"
                                      direction="column"
                                      color="#565555"
                                      justifyContent="center">
                                      <Text
                                        fontWeight="600"
                                        marginBottom="8px"
                                        fontSize={{ base: "11px", md: "16px" }}>USERNAME</Text>
                                      <Text
                                        marginBottom={{ base: "15px", md: "20px" }}
                                        fontSize={{ base: "14px", md: "16px" }}>
                                        {state.username}
                                      </Text>
                                      <Text fontWeight="600" marginBottom="8px" fontSize={{ base: "11px", md: "16px" }}>REFERRAL'S ID</Text>
                                      <Input
                                        maxW="400px"
                                        background="#D5D5D5 0% 0% no-repeat padding-box"
                                        boxShadow="inset 0px 3px 6px #00000017"
                                        borderRadius="10px"
                                        border="none"
                                        height={{ base: "27px", md: "37px" }}
                                        marginBottom={{ base: "37px", md: "52px" }}
                                      />
                                      <Button
                                        width={{ base: "100%", md: "226px" }}
                                        height="55px"
                                        background="transparent linear-gradient(76deg, #FF7495 0%, #A0DF86 100%) 0% 0% no-repeat padding-box"
                                        borderRadius="11px"
                                      >Mint NFT</Button>
                                    </Flex>
                                    <Box
                                      height="100%"
                                      width="300px"
                                    >
                                      <Image
                                        marginLeft="auto"
                                        marginRight="auto"
                                        fit="contain"
                                        maxHeight="328px"
                                        src={CryticalBlack}
                                      />
                                    </Box>

                                  </Flex>

                                  <Text
                                    color="#565555"
                                    fontSize="18px"
                                    fontWeight="500"
                                    marginTop={{ base: "34px", md: "100px" }}>Your NFT (Tier's) Top 3 Leader Board</Text>
                                  <Image
                                    width="100px"
                                    marginLeft="auto"
                                    marginRight="auto"
                                    src={CryticalBlack}
                                    display={{ base: "block", md: "none" }}
                                  />
                                  <CryticalTable
                                    stretch={true}
                                    width="100%"
                                    labels={['#', 'USERNAME', 'NEW REFERRALS (THIS WEEK)', 'STAKED AMOUNT']}
                                    data={tData !== null ? tData : []}
                                  />
                                </Flex>
                                <Text
                                  display={{ base: "inline-block", lg: "none" }}
                                  marginTop="38px"
                                  fontSize="18px"
                                  color="#565555"
                                  marginBottom="18px">My NFT</Text>
                                <Flex
                                  direction={{ base: "row", xl: "column" }}
                                  gap={{ base: "10px", lg: "37px" }}
                                  justifyContent={{ base: "space-between", xl: "flex-start" }}
                                  height="auto"
                                  paddingRight={{ lg: "28px", xl: "0px" }}>
                                  <NftInfoBox
                                    title="User’s Referral ID"
                                    subtitle={state.refferalId}
                                  />
                                  <NftInfoBox
                                    title="Invite Friends"
                                    subtitle="Create a referral link"
                                  />
                                </Flex>
                              </Flex>

                            </Flex>

                          </TabPanel>
                          <TabPanel paddingLeft="25px" width="100%">
                            <Tabs>
                              <TabList marginLeft="0px" border="none" color="#565555"
                                fontWeight="500" colorScheme="#565555" maxW="100%"
                                overflowX="scroll" overflowY="hidden">
                                <Tab color="#bababa" _selected={{ color: '#565555', fontWeight: "500" }}>ALL</Tab>
                                <Tab color="#bababa" _selected={{ color: '#565555', fontWeight: "500" }}>BLACK</Tab>
                                <Tab color="#bababa" _selected={{ color: '#565555', fontWeight: "500" }}>DIAMOND</Tab>
                                <Tab color="#bababa" _selected={{ color: '#565555', fontWeight: "500" }}>GOLD</Tab>
                                <Tab color="#bababa" _selected={{ color: '#565555', fontWeight: "500" }}>SILVER</Tab>
                                <Tab color="#bababa" _selected={{ color: '#565555', fontWeight: "500" }}>BRONZE</Tab>
                              </TabList>
                              <TabPanels maxWidth="100%" width="100%" flex="1" >
                                <TabPanel width="100%">
                                  <CryticalGrid />
                                </TabPanel>
                                <TabPanel width="100%">
                                  <CryticalGrid />
                                </TabPanel>
                                <TabPanel width="100%">
                                  <CryticalGrid />
                                </TabPanel>
                                <TabPanel width="100%">
                                  <CryticalGrid />
                                </TabPanel>
                                <TabPanel width="100%">
                                  <CryticalGrid />
                                </TabPanel>
                                <TabPanel width="100%">
                                  <CryticalGrid />
                                </TabPanel>
                              </TabPanels>
                            </Tabs>
                          </TabPanel>
                        </TabPanels>

                      </Tabs>
                    </Flex>
                  </Flex>
                </Flex>
              </Flex>

            </Flex>

          </Box>
          : null
      }
    </>

  );
}

Nft.propTypes = {};

Nft.defaultProps = {};

export default Nft;
