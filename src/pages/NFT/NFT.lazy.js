import React, { lazy, Suspense } from 'react';

const LazyNft = lazy(() => import('./Nft'));

const Nft = props => (
  <Suspense fallback={null}>
    <LazyNft {...props} />
  </Suspense>
);

export default Nft;
